# Update 5/21/2020: This version of Spiral is deprecated. If you'd like to contribute to Spiral, please see spiral-native at https://gitlab.com/TechyMitch1/spiral-native.



# Spiral - An open-source, free-form note-taking application
### © 2019 - Mitchell Augustin

##### What is Spiral?
Spiral is a program that allows users to take notes in a format similar to a spiral notebook. Unlike traditional word processors and text editors that are configured in a way that makes it difficult to document things in a non-linear fashion, Spiral pages allow users to click anywhere to start typing and easily move elements of their page around the screen. Additionally, all Spiral content is stored in plain HTML files, making it easy for users to view, edit, and export their content from outside of Spiral if they wish.

##### Features
- Non-linear document structure
- HTML-based document scheme allows for rich formatting
- Customize the UI however you please simply by editing the styles.css and layout.fxml files
- Files are auto-saved, so you don't have to worry about losing your changes
- More to come

##### Is Spiral a finished product?
Although Spiral is now available to the public, its feature set is quite minimal right now since I mainly only wrote it for myself so I could have a freeform note editor that works on Linux. However, I plan on adding more features and bug-fixes, and I invite anyone who would like to help out to contribute to the project as well.

##### How can I contribute?
If you'd like to contribute to Spiral, you can start by checking out the Issues page at https://gitlab.com/TechyMitch1/spiral/issues. If you find something you think you may be able to help with, simply clone the project, create a new branch for your patch, and start programming. Once you're finished, submit a merge request, and I'll merge it into the master branch if everything works out. You can also contribute by reporting errors you find and/or feature requests through the issues page.

##### Can I fork/redistribute/modify Spiral?
Spiral is licensed under the Apache License v. 2.0. Read the included LICENSE.txt or visit https://www.apache.org/licenses/LICENSE-2.0 for more information.