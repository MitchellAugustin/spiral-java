package com.mitchellaugustin.notebook.layout;

import com.mitchellaugustin.notebook.utils.DraggableNodeHandler;
import com.mitchellaugustin.notebook.utils.FileManager;
import com.mitchellaugustin.notebook.utils.Log;
import com.mitchellaugustin.notebook.utils.SaveFile;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Handles all file I/O within the program
 *
 * @author Mitchell Augustin
 * @version 11-05-2019
 */
public class FileHandler {
    private ResourceHandler resourceHandler;

    /**
     * Initializes the FileHandler object with the given ResourceHandler
     * @param resourceHandler
     */
    public FileHandler(ResourceHandler resourceHandler) {
        this.resourceHandler = resourceHandler;
    }

    /**
     * Opens the file chooser, allowing the user to select a location for their new notebook
     * Since notebook directories will only be created when a new one is open, this will run the file chooser for the location of the notebook.
     */
    public void newNotebookDialog() throws ParseException {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select a location for your notebook");
        File directory = directoryChooser.showDialog(resourceHandler.getNotebookFXMLController().stage);

        String notebookName = JOptionPane.showInputDialog("Enter a name for your notebook:");

        if (!notebookName.isEmpty()) {
            //Generate the file structure for the notebook
            String dirPath = directory.getAbsolutePath();
            newNotebookObject(dirPath, resourceHandler, notebookName);
        }

    }

    /**
     * Creates a new notebook object and adds it to the session.
     * @param location - The directory where the notebook will be stored
     * @param resourceHandler
     * @param notebookName - The name of the notebook
     * @throws ParseException
     */
    public void newNotebookObject(String location, ResourceHandler resourceHandler, String notebookName) throws ParseException {
        Notebook testNotebook = new Notebook(resourceHandler, location);
        testNotebook.setName(notebookName);


        resourceHandler.addNotebookToList(testNotebook);

        //Create the directory and save the default notebookManifest
        String uuidString = testNotebook.getUUID().toString();
        location = location + File.separator + "notebook-" + uuidString;
        File notebookDir = new File(location);
        Log.info("Generating notebook directory at " + location);
        notebookDir.mkdir();


        ArrayList<Object> subUUIDs = new ArrayList<>();
        String notebookManifestString = updateManifest(notebookName, testNotebook.getUUID().toString(), subUUIDs);
        if (location != null) {
            SaveFile.saveFile(location + File.separator + "notebookManifest.json", notebookManifestString);
        }

        newSectionObject(location, resourceHandler, "New Section", testNotebook);

        resourceHandler.openNotebook(testNotebook);
        addNotebookToSession(testNotebook);
    }

    /**
     * Opens a dialog box that allows the user to create a new section in the parameterized notebook.
     * @param notebook - The Notebook where the new section will be created.
     * @throws ParseException
     */
    public void newSectionDialog(Notebook notebook) throws ParseException {
        Log.info("New section for notebook: " + notebook.getName());
        File directory = new File(notebook.getPath());

        String sectionName = JOptionPane.showInputDialog("Enter a name for your section:");

        if (!sectionName.isEmpty()) {
            //Generate the file structure for the notebook
            String dirPath = directory.getAbsolutePath();
            newSectionObject(dirPath, resourceHandler, sectionName, notebook);
        }

    }

    /**
     * Creates a new section object and adds it to the parameterized notebook and session
     * @param location - The directory of the parent notebook
     * @param resourceHandler
     * @param sectionName - The name of the new section
     * @param notebook - The parent Notebook object
     * @throws ParseException
     */
    public void newSectionObject(String location, ResourceHandler resourceHandler, String sectionName, Notebook notebook) throws ParseException {
        NotebookSection section = new NotebookSection(resourceHandler, notebook);
        section.setName(sectionName);


        notebook.getSections().add(section);
        resourceHandler.addSectionToList(section);


        //Create the directory and save the default notebookManifest
        String uuidString = section.getUUID().toString();
        String sectionLocation = location + File.separator + "section-" + uuidString;
        File sectionDir = new File(sectionLocation);
        Log.info("Generating section directory at " + sectionLocation);
        sectionDir.mkdir();

        ArrayList<Object> subUUIDs = new ArrayList<>();
        String sectionManifestString = updateManifest(sectionName, section.getUUID().toString(), subUUIDs);
        if (sectionLocation != null) {
            SaveFile.saveFile(sectionLocation + File.separator + "sectionManifest.json", sectionManifestString);
        }

        List<Object> notebookManifestData = getManifestData(location + File.separator + "notebookManifest.json");
        if (notebookManifestData == null) {
            return;
        }
        ArrayList<Object> sectionUUIDs = (ArrayList<Object>) notebookManifestData.get(2);
        sectionUUIDs.add(uuidString);
        String notebookManifestString = updateManifest((String) notebookManifestData.get(0), (String) notebookManifestData.get(1), sectionUUIDs);

        if (location != null) {
            SaveFile.saveFile(location + File.separator + "notebookManifest.json", notebookManifestString);
        }

        newPageObject(sectionLocation, resourceHandler, "New Page", notebook, section);

        resourceHandler.setCurrentSection(section, notebook, true);
    }

    /**
     * Removes a NotebookSection from its parent notebook
     * @param section
     * @throws ParseException
     */
    public void removeSection(NotebookSection section) throws ParseException {
        String notebookManifestLocation = section.getParentNotebook().getPath() + File.separator + "notebookManifest.json";
        List<Object> notebookManifestData = getManifestData(notebookManifestLocation);
        if (notebookManifestData == null) {
            return;
        }
        ArrayList<Object> sectionUUIDs = (ArrayList<Object>) notebookManifestData.get(2);
        sectionUUIDs.remove(section.getUUID().toString());
        String notebookManifestString = updateManifest((String) notebookManifestData.get(0), (String) notebookManifestData.get(1), sectionUUIDs);

        if (notebookManifestLocation != null) {
            SaveFile.saveFile(notebookManifestLocation, notebookManifestString);
        }
        Log.info("Removed section from notebookManifest at " + notebookManifestLocation);
    }

    /**
     * Opens a dialog box that allows the user to create a new page in the parameterized NotebookSection
     * @param notebook - The parent Notebook
     * @param notebookSection - The parent NotebookSection
     * @throws ParseException
     */
    public void newPageDialog(Notebook notebook, NotebookSection notebookSection) throws ParseException {
        Log.info("New page for notebookSection: " + notebookSection.getName());
        File directory = new File(notebook.getPath());

        String pageName = JOptionPane.showInputDialog("Enter a name for your page:");

        if (!pageName.isEmpty()) {
            //Generate the file structure for the page
            String dirPath = directory.getAbsolutePath() + File.separator + "section-" + notebookSection.getUUID().toString();
            newPageObject(dirPath, resourceHandler, pageName, notebook, notebookSection);
        }

    }

    /**
     * Creates a new section object, adds it to the parameterized section, notebook, and session, and saves it.
     * @param location - The directory of the parent section
     * @param resourceHandler
     * @param pageName - The name of the new page
     * @param notebook - The parent Notebook object
     * @param section  - The parent NotebookSection Object
     * @throws ParseException
     */
    public void newPageObject(String location, ResourceHandler resourceHandler, String pageName, Notebook notebook, NotebookSection section) throws ParseException {
        NotebookPage page = new NotebookPage(resourceHandler, section);
        page.setName(pageName);

        resourceHandler.setCurrentPage(page);
        section.getPages().add(page);
        resourceHandler.getNotebookFXMLController().addPage(page);


        //Create the directory and save the default notebookManifest
        String uuidString = page.getUUID().toString();
        String pageLocation = location + File.separator + "page-" + uuidString;
        File pageDir = new File(pageLocation);
        Log.info("Generating page directory at " + pageLocation);
        pageDir.mkdir();


        ArrayList<Object> subUUIDs = new ArrayList<>();
        String pageManifestString = updateManifest(pageName, page.getUUID().toString(), subUUIDs);
        if (pageLocation != null) {
            SaveFile.saveFile(pageLocation + File.separator + "pageManifest.json", pageManifestString);
        }

        List<Object> sectionManifestData = getManifestData(location + File.separator + "sectionManifest.json");
        if (sectionManifestData == null) {
            return;
        }
        ArrayList<Object> pageUUIDs = (ArrayList<Object>) sectionManifestData.get(2);
        pageUUIDs.add(uuidString);
        pageManifestString = updateManifest((String) sectionManifestData.get(0), (String) sectionManifestData.get(1), pageUUIDs);

        if (location != null) {
            SaveFile.saveFile(location + File.separator + "sectionManifest.json", pageManifestString);
        }

    }

    /**
     * Removes a NotebookPage from its parent NotebookSection's sectionManifest.json file
     * @param page - The page to remove
     * @throws ParseException
     */
    public void removePage(NotebookPage page) throws ParseException {
        String sectionManifestLocation = page.getParentNotebookSection().getParentNotebook().getPath() + File.separator + "section-" + page.getParentNotebookSection().getUUID().toString() + File.separator + "sectionManifest.json";
        List<Object> sectionManifestData = getManifestData(sectionManifestLocation);
        ArrayList<Object> pageUUIDs = (ArrayList<Object>) sectionManifestData.get(2);
        pageUUIDs.remove(page.getUUID().toString());
        String notebookManifestString = updateManifest((String) sectionManifestData.get(0), (String) sectionManifestData.get(1), pageUUIDs);

        if (sectionManifestLocation != null) {
            SaveFile.saveFile(sectionManifestLocation, notebookManifestString);
        }
        Log.info("Removed page from sectionManifest at " + sectionManifestLocation);
    }

    /**
     * Saves the contents of the parameterized NotebookPageLayer to its corresponding HTML file on the drive
     * @param layer - The NotebookPageLayer to save
     * @throws ParseException
     * Note: Since new page layers can only be created on the current page, the path info will come from the getCurrent[whatever] methods.
     */
    public void updatePageLayer(NotebookPageLayer layer) throws ParseException {
        /**
         * 1. Save the HTML file to the page directory
         * 2. Update the pageManifest to include the pageLayer UUID and its location
         */
        String pageDirectory = resourceHandler.getCurrentNotebook().getPath() + File.separator + "section-" + resourceHandler.getCurrentNotebookSection().getUUID().toString() + File.separator + "page-" + resourceHandler.getCurrentNotebookPage().getUUID().toString();
        String thisPageManifest = pageDirectory + File.separator + "pageManifest.json";
        String thisLayerHTML = pageDirectory + File.separator + layer.getUUID().toString() + ".html";

        Document doc = Jsoup.parse(layer.getTextBox().getController().getText());

        if (!doc.select("body").first().html().isEmpty()) {
            SaveFile.saveFile(thisLayerHTML, layer.getTextBox().getController().getText());


            ArrayList<Object> subUUIDs = (ArrayList<Object>) getManifestData(thisPageManifest).get(2);
            if (subUUIDs == null) {
                return;
            }
            for (int i = 0; i < subUUIDs.size(); i++) {
                Map<String, String> obj = (Map<String, String>) subUUIDs.get(i);
                if (obj.get(layer.getUUID().toString()) != null) {
                    //If this object is already in the manifest, remove it so a duplicate object won't be created.
                    subUUIDs.remove(i);
                }
            }

            Map<String, String> layerManifestObject = new HashMap<>();
            layerManifestObject.put(layer.getUUID().toString(), layer.getCoords().getX() + "," + layer.getCoords().getY() + "|" + layer.getTextBox().getController().getHtmlEditor().getWidth() + "x" + layer.getTextBox().getController().getHtmlEditor().getHeight());
            subUUIDs.add(layerManifestObject);
            String newPageManifest = updateManifest(resourceHandler.getCurrentNotebookPage().getName(), resourceHandler.getCurrentNotebookPage().getUUID().toString(), subUUIDs);
            SaveFile.saveFile(thisPageManifest, newPageManifest);
        }
    }

    /**
     * Removes a NotebookPageLayer from its parent NotebookPage
     * @param layer
     * @throws ParseException
     */
    public void removePageLayer(NotebookPageLayer layer) throws ParseException {
        String parentNotebookPath = layer.getParentNotebookPage().getParentNotebookSection().getParentNotebook().getPath();
        String pageManifestLocation = parentNotebookPath + File.separator + "section-" + layer.getParentNotebookPage().getParentNotebookSection().getUUID().toString() + File.separator + "page-" + layer.getParentNotebookPage().getUUID().toString() + File.separator + "pageManifest.json";
        List<Object> pageManifestData = getManifestData(pageManifestLocation);
        if (pageManifestData == null) {
            return;
        }
        ArrayList<Object> pageLayerUUIDs = (ArrayList<Object>) pageManifestData.get(2);
        for (int i = 0; i < pageLayerUUIDs.size(); i++) {
            Map<String, String> currentLayerMap = (Map) pageLayerUUIDs.get(i);
            if (currentLayerMap != null && currentLayerMap.get(layer.getUUID().toString()) != null && !currentLayerMap.get(layer.getUUID().toString()).isEmpty()) {
                pageLayerUUIDs.remove(i);
            }
        }
        String pageManifestString = updateManifest((String) pageManifestData.get(0), (String) pageManifestData.get(1), pageLayerUUIDs);

        if (pageManifestLocation != null) {
            SaveFile.saveFile(pageManifestLocation, pageManifestString);
        }
        Log.info("Removed PageLayer from pageManifest at " + pageManifestLocation);
    }

    /**
     * Opens a dialog box that allows the user to open a saved notebook
     * @throws ParseException
     * @throws IOException
     */
    public void openNotebookDialog() throws ParseException, IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select your notebook's notebookManifest.json file");
        File manifestFile = fileChooser.showOpenDialog(resourceHandler.getNotebookFXMLController().stage);
        openNotebookFromFile(manifestFile.getAbsolutePath());
        Log.info("Opening " + manifestFile.getAbsolutePath());
    }

    /**
     * Opens the notebook with the given manifest in the window.
     * This will be called from a successful execution of openNotebookDialog() AND within resourceHandler once the session system is implemented.
     * 1. Parse the notebookManifest file.
     * 2. Create a Notebook object for the notebook with the correct metadata.
     * 3. Add the Notebook to the session (To be implemented later)
     * 4. Add the Notebook title to the Notebook sidebar.
     * -For each section:
     * a. Parse the sectionManifest file
     * b. Create a NotebookSection object for the section with the correct metadata
     * c. Add the NotebookSection to the parent Notebook
     * d. Add the Section title to the Section sidebar.
     * -For each page:
     * a. Parse the pageManifest file.
     * b. Create a NotebookPage object for the page with the correct metadata
     * c. Add the NotebookPage to the parent NotebookSection
     * d. Create a new JavaFX Tab for the page with the correct title.
     * e. Get all PageLayers, their coords, and their content
     * f. Add a new TextBox for each layer at the specified location on its correct page.
     * @param manifestLocation - The path to notebookManifest.json file for this notebook
     * NOTE: In order to allow notebooks to be moved, the static paths to included files are updated for the current system in this method.
     */
    public void openNotebookFromFile(String manifestLocation) throws ParseException, IOException {
        String notebookDirectory = manifestLocation.replace(File.separator + "notebookManifest.json", "");
        List<Object> notebookManifestInfo = getManifestData(manifestLocation);
        if (notebookManifestInfo == null) {
            return;
        }
        //For each section
        ArrayList<Object> sectionUUIDs = (ArrayList<Object>) notebookManifestInfo.get(2);

        Notebook notebook = new Notebook(resourceHandler, notebookDirectory);
        notebook.setName((String) notebookManifestInfo.get(0));
        notebook.setUUID((String) notebookManifestInfo.get(1));
        resourceHandler.setCurrentNotebook(notebook, false);

        for (Object sectionUUIDObj : sectionUUIDs) {
            String sectionUUIDString = (String) sectionUUIDObj;
            String sectionDirectory = notebookDirectory + File.separator + "section-" + sectionUUIDString;
            String sectionManifestLocation = sectionDirectory + File.separator + "sectionManifest.json";
            List<Object> sectionManifestInfo = getManifestData(sectionManifestLocation);
            if (sectionManifestInfo == null) {
                continue;
            }
            //For each page in the section
            ArrayList<Object> pageUUIDs = (ArrayList<Object>) sectionManifestInfo.get(2);

            NotebookSection section = new NotebookSection(resourceHandler, notebook);
            section.setName((String) sectionManifestInfo.get(0));
            section.setUUID((String) sectionManifestInfo.get(1));
            resourceHandler.setCurrentSection(section, notebook, false);

            for (Object pageUUIDObj : pageUUIDs) {
                String pageUUIDString = (String) pageUUIDObj;
                String pageDirectory = sectionDirectory + File.separator + "page-" + pageUUIDString;
                String pageManifestLocation = pageDirectory + File.separator + "pageManifest.json";
                List<Object> pageManifestInfo = getManifestData(pageManifestLocation);
                if (pageManifestInfo == null) {
                    continue;
                }

                NotebookPage page = new NotebookPage(resourceHandler, section);
                page.setName((String) pageManifestInfo.get(0));
                page.setUUID((String) pageManifestInfo.get(1));

                section.getPages().add(page);
                resourceHandler.getNotebookFXMLController().addPage(page);
                resourceHandler.setCurrentPage(page);

                ArrayList<Object> pageLayerUUIDs = (ArrayList<Object>) pageManifestInfo.get(2);
                for (Object pageLayerUUIDObj : pageLayerUUIDs) {
                    Map<String, String> pageLayerUUIDMap = (Map<String, String>) pageLayerUUIDObj;
                    Iterator it = pageLayerUUIDMap.entrySet().iterator();
                    while (it.hasNext()) {
                        Log.info("Iterating through map");
                        Map.Entry pair = (Map.Entry) it.next();
                        String pageLayerUUID = pair.getKey().toString();
                        String pageLayerCoordString = pair.getValue().toString().split("\\|")[0].replace("|", "");
                        String pageLayerSizeString = pair.getValue().toString().split("\\|")[1].replace("|", "");
                        Point pageLayerPoint = new Point((int) Double.parseDouble(pageLayerCoordString.split(",")[0]), (int) Double.parseDouble(pageLayerCoordString.split(",")[1]));
//                        page.newPageLayer(pageLayerPoint);
                        double pageLayerSizeX = Double.parseDouble(pageLayerSizeString.split("x")[0]);
                        double pageLayerSizeY = Double.parseDouble(pageLayerSizeString.split("x")[1]);
                        Point pageLayerSizePoint = new Point((int) pageLayerSizeX, (int) pageLayerSizeY);

                        String layerHTMLString = SaveFile.readFile(pageDirectory + File.separator + pageLayerUUID + ".html");
                        DraggableNodeHandler handler = null;
                        try {
                            Log.info("Creating layer at " + pageLayerPoint.toString());
                            handler = resourceHandler.getNotebookFXMLController().addNewPageLayer(page, pageLayerPoint, pageLayerSizePoint, page.getEditorPane(), pageLayerUUID, false);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        //Replaces static link with valid link from current system (Should probably come up with a better solution later)
                        layerHTMLString = layerHTMLString.replaceAll(Pattern.quote("file:///") + ".*" + Pattern.quote("/") + "section", "file:///" + notebook.getPath().replace(File.separator, "/") + "/section");
                        page.getPageLayers().get(page.getPageLayers().size() - 1).getTextBox().getController().setText(layerHTMLString);
                        it.remove();
                    }
                }
            }

            notebook.getSections().add(section);
        }

        resourceHandler.openNotebook(notebook);
        resourceHandler.addNotebookToList(notebook);
        addNotebookToSession(notebook);
    }

    /**
     * Opens the notebooks contained in the app directory's session.json file
     * @throws ParseException
     * @throws IOException
     */
    public void openNotebooksFromSessionFile() throws ParseException, IOException {
        FileManager fileManager = new FileManager();
        String jsonFile = SaveFile.readFile(fileManager.getSessionFileLocation());
        if (jsonFile == null || jsonFile.isEmpty() || jsonFile.equals("Unavailible")) {
            String defaultSessionFile = "{\"notebookDirs\": [\"\"]}";
            SaveFile.saveFile(fileManager.getSessionFileLocation(), defaultSessionFile);
            jsonFile = defaultSessionFile;
        }
        JSONParser parser = new JSONParser();
        JSONObject fullObject = (JSONObject) parser.parse(jsonFile);

        ArrayList<Object> result = (ArrayList<Object>) fullObject.get("notebookDirs");

        for (Object o : result) {
            String fileString = (String) o;
            if (fileString != null && !fileString.isEmpty()) {
                openNotebookFromFile(fileString);
            }
        }
    }

    /**
     * Adds the parameterized Notebook object to the session file
     * @param notebook
     * @throws ParseException
     */
    public void addNotebookToSession(Notebook notebook) throws ParseException {
        //Save the notebook's notebookManifest path to the session file
        FileManager fileManager = new FileManager();
        String jsonFile = SaveFile.readFile(fileManager.getSessionFileLocation());

        JSONParser parser = new JSONParser();
        JSONObject fullObject = (JSONObject) parser.parse(jsonFile);

        ArrayList<Object> result = new ArrayList<>();
        result = (ArrayList<Object>) fullObject.get("notebookDirs");
        boolean contains = false;
        for (Object o : result) {
            String str = (String) o;
            if (str.contains(notebook.getPath())) {
                contains = true;
                break;
            }
        }
        if (!contains) {
            result.add(notebook.getPath() + File.separator + "notebookManifest.json");
        }


        fullObject.put("notebookDirs", result);

        SaveFile.saveFile(fileManager.getSessionFileLocation(), fullObject.toJSONString());
    }

    /**
     * Removes the parameterized Notebook object from the session file
     * @param notebook
     * @throws ParseException
     */
    public void removeNotebookFromSession(Notebook notebook) throws ParseException {
        //Remove the notebook's notebookManifest path from the session file
        FileManager fileManager = new FileManager();
        String jsonFile = SaveFile.readFile(fileManager.getSessionFileLocation());

        JSONParser parser = new JSONParser();
        JSONObject fullObject = (JSONObject) parser.parse(jsonFile);

        ArrayList<Object> result = (ArrayList<Object>) fullObject.get("notebookDirs");

        Iterator<Object> iter = result.iterator();

        while (iter.hasNext()) {
            String str = (String) iter.next();

            if (str.contains(notebook.getPath())) {
                Log.info("Removing notebook: " + str);
                iter.remove();
            }
        }

        JSONObject newObject = new JSONObject();

        newObject.put("notebookDirs", result);

        SaveFile.saveFile(fileManager.getSessionFileLocation(), newObject.toJSONString());
    }

    /**
     * Updates the name of the parameterized Notebook object in its file
     * @param notebook
     * @param newName
     * @throws ParseException
     */
    public void renameNotebook(Notebook notebook, String newName) throws ParseException {
        String manifestLocation = notebook.getPath() + File.separator + "notebookManifest.json";
        List<Object> manifestData = getManifestData(manifestLocation);
        if (manifestData == null) {
            return;
        }
        SaveFile.saveFile(manifestLocation, updateManifest(newName, (String) manifestData.get(1), (ArrayList<Object>) manifestData.get(2)));
    }

    /**
     * Updates the name of the current section in its file
     * @param newName
     * @throws ParseException
     */
    public void renameSection(String newName) throws ParseException {
        String manifestLocation = resourceHandler.getCurrentNotebook().getPath() + File.separator + "section-" + resourceHandler.getCurrentNotebookSection().getUUID().toString() + File.separator + "sectionManifest.json";
        List<Object> manifestData = getManifestData(manifestLocation);
        if (manifestData == null) {
            return;
        }
        SaveFile.saveFile(manifestLocation, updateManifest(newName, (String) manifestData.get(1), (ArrayList<Object>) manifestData.get(2)));
    }

    /**
     * Updates the name of the current page in its file
     * @param newName
     * @throws ParseException
     */
    public void renamePage(String newName) throws ParseException {
        String manifestLocation = resourceHandler.getCurrentNotebook().getPath() + File.separator + "section-" + resourceHandler.getCurrentNotebookSection().getUUID().toString() + File.separator + "page-" + resourceHandler.getCurrentNotebookPage().getUUID().toString() + File.separator + "pageManifest.json";
        List<Object> manifestData = getManifestData(manifestLocation);
        if (manifestData == null) {
            return;
        }
        SaveFile.saveFile(manifestLocation, updateManifest(newName, (String) manifestData.get(1), (ArrayList<Object>) manifestData.get(2)));
    }

    /**
     * Returns an Object List containing the data in the manifest file at the parameterized location
     * Format:
     * list.get(0): A String containing the entity name
     * list.get(1): A String containing the entity UUID
     * list.get(2): An {@code ArrayList<Object>} containing the UUIDs of all sub-entities
     *     -These Objects are Strings in all cases except for NotebookPages, where they are instead maps containing the UUID and the coordinates of the sub NotebookPageLayer.
     * @param manifestFileLocation
     * @return
     * @throws ParseException
     */
    private List<Object> getManifestData(String manifestFileLocation) throws ParseException {
        String jsonFile = SaveFile.readFile(manifestFileLocation);

        if (jsonFile.equals("Unavailible")) {
            JOptionPane.showMessageDialog(null, "Error: File Not Found: " + manifestFileLocation, "Error", JOptionPane.ERROR_MESSAGE);
            return null;
        }

        JSONParser parser = new JSONParser();
        JSONObject fullObject = (JSONObject) parser.parse(jsonFile);

        ArrayList<Object> result = new ArrayList<>();
        result.add(fullObject.get("name"));
        result.add(fullObject.get("uuid"));
        result.add(fullObject.get("subUUIDs"));

        return result;
    }

    /**
     * Returns a JSON String updated with the parameterized values for the manifest files
     * @param objName
     * @param uuid
     * @param subUUIDs
     * @return
     */
    private String updateManifest(String objName, String uuid, ArrayList<Object> subUUIDs) {
        JSONObject fullObject = new JSONObject();

        fullObject.put("name", objName);
        fullObject.put("uuid", uuid);
        fullObject.put("subUUIDs", subUUIDs);

        return fullObject.toJSONString();
    }
}
