package com.mitchellaugustin.notebook.layout;

import com.mitchellaugustin.notebook.utils.Log;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Structural class for notebooks containing all metadata and child sections
 *
 * @author Mitchell Augustin
 */
public class Notebook {
    public static final String CLASS_NAME = "Notebook";
    private static int sectionsInNotebook = 0;

    private ResourceHandler resourceHandler;
    private String name;
    private UUID uuid;
    private ArrayList<NotebookSection> sections;
    private String path;

    /**
     * Instantiates a new Notebook object with the provided ResourceHandler and path
     *
     * @param resourceHandler  - The ResourceHandler object to use
     * @param notebookLocation - The Notebook's path
     */
    public Notebook(ResourceHandler resourceHandler, String notebookLocation) {
        Log.info("New notebook initialized at " + notebookLocation, CLASS_NAME);
        sections = new ArrayList<>();
        this.resourceHandler = resourceHandler;
        init(notebookLocation);
    }

    /**
     * Initializes the notebook with a random UUID and assigns its path
     *
     * @param notebookLocation
     */
    private void init(String notebookLocation) {
        uuid = UUID.randomUUID();
        if (notebookLocation.contains("notebook-")) {
            path = notebookLocation;
        } else {
            path = notebookLocation + File.separator + "notebook-" + uuid.toString();
        }
    }

    /**
     * Returns the name of the Notebook
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the UUID of the Notebook
     * @return
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Sets the UUID of the Notebook
     * @param uuidString
     */
    public void setUUID(String uuidString) {
        uuid = UUID.fromString(uuidString);
    }

    /**
     * Returns the file path of this Notebook
     * @return
     */
    public String getPath() {
        return path;
    }

    /**
     * Returns an ArrayList of all child NotebookSection objects
     * @return
     */
    public ArrayList<NotebookSection> getSections() {
        return sections;
    }

    /**
     * Searches for and returns a NotebookSection by its UUID
     * @param uuid
     * @return
     */
    public NotebookSection getSectionByUUID(UUID uuid) {
        for (NotebookSection section : sections) {
            if (uuid.compareTo(section.getUUID()) == 0) {
                return section;
            }
        }
        return null;
    }

    /**
     * Sets the name of the Notebook
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Creates a new NotebookSection and adds it to the list
     */
    public void newSection() {
        NotebookSection section = new NotebookSection(resourceHandler, this);
        section.setName("New Section " + sectionsInNotebook);
        sections.add(section);
        resourceHandler.getNotebookFXMLController().addSectionToList(section);
        sectionsInNotebook++;
    }

    /**
     * Removes the NotebookSection with the provided UUID from the Notebook
     * @param uuid
     */
    public void removeSection(UUID uuid) {
        for (int i = 0; i < sections.size(); i++) {
            if (sections.get(i).getUUID().compareTo(uuid) == 0) {
                try {
                    resourceHandler.getFileHandler().removeSection(sections.get(i));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                sections.remove(sections.get(i));
            }
        }
        sectionsInNotebook--;
    }
}
