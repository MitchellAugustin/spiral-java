package com.mitchellaugustin.notebook.layout;

import com.mitchellaugustin.notebook.utils.Constants;
import com.mitchellaugustin.notebook.utils.DraggableNodeHandler;
import com.mitchellaugustin.notebook.utils.Log;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import org.json.simple.parser.ParseException;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Handles all UI interactions under the domain of the main layout window.
 * Note: Specific UI controls for the movable text boxes themselves are controlled by TextBoxFXMLController
 *
 * @author Mitchell Augustin
 */
public class NotebookFXMLController {
    public static final String CLASS_NAME = "NotebookFXMLController";

    Stage stage;
    @SuppressWarnings("unused")
    private ResourceHandler resourceHandler;

    public NotebookFXMLController(Stage primaryStage, ResourceHandler resourceHandler) {
        stage = primaryStage;
        this.resourceHandler = resourceHandler;
    }

    @FXML
    private TabPane pagePane;

    @FXML
    private ListView<String> notebooksList;

    @FXML
    private ListView<String> sectionsList;

    private Point currentCoordinates;

    @FXML
    /**
     * Initializer (called by the FXMLLoader)
     */
    private void initialize() {
        Log.info(pagePane.getLayoutBounds().getWidth() + ", " + pagePane.getLayoutBounds().getHeight(), CLASS_NAME);

        //UI Controls for page removal
        pagePane.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.DELETE)) {
                    removePage(pagePane.getSelectionModel().getSelectedIndex(), resourceHandler.getCurrentNotebookPage());
                    //Sets the next page in line as the currentPage when one is deleted
                    resourceHandler.setCurrentPage(resourceHandler.getCurrentNotebookSection().getPages().get(pagePane.getSelectionModel().getSelectedIndex()));
                }
            }
        });

        //Changes the current page to the proper tab whenever the selection is changed
        pagePane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
            @Override
            public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
                int selectedIndex = pagePane.getSelectionModel().getSelectedIndex();
                if (selectedIndex > -1 && resourceHandler.getCurrentNotebookSection() != null && resourceHandler.getCurrentNotebookSection().getPages() != null) {
                    Log.info("Selection changed, current (page) tabIndex is " + selectedIndex);
                    resourceHandler.setCurrentPage(resourceHandler.getCurrentNotebookSection().getPages().get(selectedIndex));
                }
            }
        });

        pagePane.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2) {
                    Log.info("PagePane double clicked");
                    try {
                        resourceHandler.renamePageDialog(resourceHandler.getCurrentNotebookPage());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        //UI Controls for Section removal
        sectionsList.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.DELETE)) {
                    removeSection(sectionsList.getSelectionModel().getSelectedIndex(), resourceHandler.getCurrentNotebookSection());
                    //Sets the next page in line as the currentPage when one is deleted
                    try {
                        resourceHandler.setCurrentSection(resourceHandler.getCurrentNotebook().getSections().get(sectionsList.getSelectionModel().getSelectedIndex()), resourceHandler.getCurrentNotebook(), true);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        //Changes the current section whenever the selection is changed
        sectionsList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                int selectedIndex = sectionsList.getSelectionModel().getSelectedIndex();

                if (selectedIndex > -1) {
                    Log.info("Selection changed, current listIndex is " + selectedIndex);
                    try {
                        resourceHandler.setCurrentSection(resourceHandler.getCurrentNotebook().getSections().get(selectedIndex), resourceHandler.getCurrentNotebook(), true);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        sectionsList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2) {
                    Log.info("sectionsList double clicked");
                    try {
                        resourceHandler.renameSectionDialog(resourceHandler.getCurrentNotebookSection());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        //UI Controls for Notebook removal
        notebooksList.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode().equals(KeyCode.DELETE)) {
                    removeNotebook(notebooksList.getSelectionModel().getSelectedIndex(), resourceHandler.getCurrentNotebook());
                }
            }
        });

        //Changes the current notebook whenever the selection is changed
        notebooksList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                int selectedIndex = notebooksList.getSelectionModel().getSelectedIndex();

                if (selectedIndex > -1) {
                    Log.info("Selection changed, current listIndex is " + selectedIndex);
                    try {
                        //Current bug: Notebook switching breaks when notebooks are opened because multiple Notebook objects are erroneously added to notebooksList.
                        resourceHandler.setCurrentNotebook(resourceHandler.getNotebooks().get(selectedIndex), true);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        notebooksList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2) {
                    Log.info("notebooksList double clicked");
                    try {
                        resourceHandler.renameNotebookDialog(resourceHandler.getCurrentNotebook());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    /**
     * Adds the specified Notebook to the UI and opens it
     * @param notebook
     */
    protected void openNotebook(Notebook notebook) {
        sectionsList.getItems().clear();
        //Note: The Notebook name should NOT be added to notebooksList here since this method is for actually opening the notebook.
        NotebookSection section1 = null;

        //Set the tab titles to those listed in the Notebook.
        pagePane.getTabs().clear();
        for (NotebookSection section : notebook.getSections()) {
            sectionsList.getItems().add(section.getName());
            if (section1 == null) {
                section1 = section;
            }
        }

        if (section1 != null && !section1.getPages().isEmpty()) {
            openSection(section1);
        }
    }

    /**
     * Opens the specified section in the UI
     * @param section
     */
    protected void openSection(NotebookSection section) {
        Log.info("Opening section " + section.getName());
        pagePane.getTabs().clear();
        //Set the page titles to those listed in the first tab
        if (section != null) {
            for (NotebookPage page : section.getPages()) {
                Tab pageTab = new Tab(page.getName());
                if (page.getEditorPane().getChildren().size() > 0) {
                    ScrollPane pageScrollPane = new ScrollPane();
                    pageScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
                    pageScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
                    pageScrollPane.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
                        @Override
                        public void handle(KeyEvent keyEvent) {
                            if(keyEvent.getCode() == KeyCode.SPACE) {
                                keyEvent.consume();
                            }
                        }
                    });
                    pageScrollPane.setContent(page.getEditorPane());
                    pageTab.setContent(pageScrollPane);
                } else {
                    pageTab.setContent(page.getEditorPane());
                }
                pagePane.getTabs().add(pageTab);
                Log.info("Page: " + page.getName() + ", Layers: " + page.getPageLayers().size());
                for (NotebookPageLayer pageLayer : page.getPageLayers()) {
                    WebView webView = (WebView) pageLayer.getTextBox().getController().getHtmlEditor().lookup(".web-view");
                    webView.setPrefSize(pageLayer.getSize().getX(), pageLayer.getSize().getY());
                }
            }
        }
    }

    /**
     * Adds the given notebook to the Notebook list in the UI.
     *
     * @param notebook
     */
    protected void addNotebookToList(Notebook notebook) {
        notebooksList.getItems().add(notebook.getName());
    }

    /**
     * Adds the given notebook section to the Notebook section list in the UI.
     *
     * @param notebookSection
     */
    protected void addSectionToList(NotebookSection notebookSection) {
        sectionsList.getItems().add(notebookSection.getName());
    }

    /**
     * Adds the given NotebookPage to the UI
     *
     * @param newPage - the NotebookPage to add. Must be initialized before calling this method.
     */
    public void addPage(NotebookPage newPage) {
        Tab newTab = new Tab(newPage.getName());
        pagePane.getTabs().add(newTab);
        newPage.setEditorPane(createEditorPane(newPage, null));
        newTab.setContent(newPage.getEditorPane());
        pagePane.getSelectionModel().select(newTab);
    }

    /**
     * Removes the given PageLayer from the UI
     * @param dragHandler - the dragHandler object corresponding to the layer that should be removed
     */
    public void removePageLayer(Node dragHandler) {
        ((Pane) dragHandler.getParent()).getChildren().remove(dragHandler);
    }

    /**
     * Removes the given NotebookPage from the UI
     * @param tabIndex - The page's corresponding tab index in the TabPane
     * @param notebookPage - The NotebookPage object to remove
     */
    public void removePage(int tabIndex, NotebookPage notebookPage) {
        pagePane.getTabs().remove(tabIndex);
        resourceHandler.getCurrentNotebookSection().removePage(notebookPage.getUUID());
        Log.info("Page " + notebookPage.getUUID() + " with name " + notebookPage.getName() + " removed.");
        Log.info("New pageList for section " + resourceHandler.getCurrentNotebookSection().getName() + " is ");
        for (NotebookPage page : resourceHandler.getCurrentNotebookSection().getPages()) {
            System.out.print(page.getName());
        }
        System.out.println();
    }

    /**
     * Removes the given NotebookSection from the UI
     * @param listIndex - The section's corresponding list index in the ListView
     * @param notebookSection - The NotebookSection object to remove
     */
    public void removeSection(int listIndex, NotebookSection notebookSection) {
        sectionsList.getItems().remove(listIndex);
        resourceHandler.getCurrentNotebook().removeSection(notebookSection.getUUID());
        Log.info("Section " + notebookSection.getUUID() + " with name " + notebookSection.getName() + " removed.");
        Log.info("New sectionList for notebook " + resourceHandler.getCurrentNotebook().getName() + " is ");
        for (NotebookSection section : resourceHandler.getCurrentNotebook().getSections()) {
            System.out.print(section.getName());
        }
        System.out.println();
    }

    /**
     * Removes the given Notebook from the UI
     * @param listIndex - The notebook's corresponding list index in the ListView
     * @param notebook - The Notebook object to remove
     */
    public void removeNotebook(int listIndex, Notebook notebook) {
        notebooksList.getItems().remove(listIndex);
        resourceHandler.getNotebooks().remove(listIndex);

        try {
            resourceHandler.getFileHandler().removeNotebookFromSession(notebook);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Sets the next page in line as the currentPage when one is deleted
        try {
            resourceHandler.setCurrentNotebook(resourceHandler.getNotebooks().get(notebooksList.getSelectionModel().getSelectedIndex()), true);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.info("Notebook " + notebook.getUUID() + " with name " + notebook.getName() + " removed.");
        Log.info("New notebookList is ");
        for (Notebook notebook1 : resourceHandler.getNotebooks()) {
            System.out.print(notebook1.getName());
        }
        System.out.println();
    }

    /**
     * Creates an editorPane to be used as the only Node of each NotebookPage's UI tab.
     * This method should not be called from anywhere except addPage(), and the resulting editorPane should be stored in the corresponding NotebookPage object.
     *
     * @param notebookPage
     * @return
     */
    private AnchorPane createEditorPane(NotebookPage notebookPage, String uuid) {
        AnchorPane editorPane = new AnchorPane();
        editorPane.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                if (event.isStillSincePress()) {
                    currentCoordinates = new Point();
                    currentCoordinates.setLocation(event.getX(), event.getY());
                    try {
                        addNewPageLayer(notebookPage, new Point((int) event.getX(), (int) event.getY()), new Point(Constants.DEFAULT_TEXTBOX_WIDTH, Constants.DEFAULT_TEXTBOX_HEIGHT), editorPane, uuid, true);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

            }

        });

        Log.info("editorPane initialized", CLASS_NAME);
        return editorPane;
    }

    /**
     * Creates a new DraggableNodeHandler containing the contents of a new NotebookPageLayer and all metadata
     * @param page
     * @param point
     * @param size
     * @param editorPane
     * @param uuid
     * @param fromClick
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public DraggableNodeHandler addNewPageLayer(NotebookPage page, Point point, Point size, AnchorPane editorPane, String uuid, boolean fromClick) throws IOException, ParseException {
        currentCoordinates = new Point();
        currentCoordinates.setLocation(point.getX(), point.getY());
        DraggableNodeHandler handler;
        handler = addTextBox(page, size, uuid, fromClick);
        Log.info("Creating TextBox at " + point.getX() + ", " + point.getY(), CLASS_NAME);
        handler.makeDraggable();
        handler.setLocation(point.getX(), point.getY());
        editorPane.getChildren().add(handler.makeUndraggable());

        if (page.getEditorPane().getChildren().size() == 1) {
            ScrollPane pageScrollPane = new ScrollPane();
            pageScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
            pageScrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
            pageScrollPane.setContent(page.getEditorPane());
            pageScrollPane.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent keyEvent) {
                    if(keyEvent.getCode() == KeyCode.SPACE) {
                        keyEvent.consume();
                    }
                }
            });
            pagePane.getTabs().get(pagePane.getSelectionModel().getSelectedIndex()).setContent(pageScrollPane);
        }
        return handler;
    }

    /**
     * Adds an editable TextBox object on its own NotebookPageLayer.
     * This method should not be called from anywhere except createEditorPane().
     *
     * @param notebookPage
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    private DraggableNodeHandler addTextBox(NotebookPage notebookPage, Point size, String uuid, boolean fromClick) throws FileNotFoundException, IOException, ParseException {
        if (uuid == null) {
            notebookPage.newPageLayer(currentCoordinates, new Point(Constants.DEFAULT_TEXTBOX_WIDTH, Constants.DEFAULT_TEXTBOX_HEIGHT));
        } else {
            notebookPage.newPageLayer(currentCoordinates, size, uuid);
        }

        NotebookPageLayer textBoxLayer = notebookPage.getPageLayers().get(notebookPage.getPageLayers().size() - 1);

        DraggableNodeHandler dragHandler = new DraggableNodeHandler(textBoxLayer, notebookPage.getEditorPane());
        textBoxLayer.setData(fromClick ? "Type here..." : "", stage, notebookPage.getEditorPane(), dragHandler);

        dragHandler.setOnDragComplete(new Runnable() {
            @Override
            public void run() {
                Log.info("Layer " + textBoxLayer.getUUID().toString() + " moved to " + dragHandler.getNodeX() + ", " + dragHandler.getNodeY(), CLASS_NAME);
                textBoxLayer.setCoords(dragHandler.getNodeX(), dragHandler.getNodeY());
                try {
                    resourceHandler.getFileHandler().updatePageLayer(textBoxLayer);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        dragHandler.setOnMousePressed(new Runnable() {
            @Override
            public void run() {
                textBoxLayer.getTextBox().getController().setToolbarVisibility(false);
            }
        });

        resourceHandler.getFileHandler().updatePageLayer(textBoxLayer);
        return dragHandler;
    }

    /**
     * Returns the ListView containing the Notebooks currently in the session
     * @return
     */
    public ListView<String> getNotebooksList() {
        return notebooksList;
    }

    /**
     * Returns the ListView containing the NotebookSections of the currently open Notebook
     * @return
     */
    public ListView<String> getSectionsList() {
        return sectionsList;
    }

    /**
     * Returns the TabPane containing the NotebookPages of the currently open NotebookSection
     * @return
     */
    public TabPane getPagePane() {
        return pagePane;
    }
}
