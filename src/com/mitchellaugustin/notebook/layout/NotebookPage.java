package com.mitchellaugustin.notebook.layout;

import com.mitchellaugustin.notebook.utils.Log;
import javafx.scene.layout.AnchorPane;
import org.json.simple.parser.ParseException;

import java.awt.*;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Structural class for notebook sections containing all metadata and child pages
 *
 * @author Mitchell Augustin
 */
public class NotebookPage {
    public static final String CLASS_NAME = "NotebookPage";

    private ResourceHandler resourceHandler;

    private NotebookSection parentNotebookSection;
    private String name;
    private UUID uuid;
    private ArrayList<NotebookPageLayer> pageLayers;
    private AnchorPane editorPane;

    /**
     * Instantiates a new NotebookPage object with the provided ResourceHandler object and parent notebook section
     *
     * @param resourceHandler       - The ResourceHandler object
     * @param parentNotebookSection - The page's parent section
     * @author Mitchell Augustin
     */
    public NotebookPage(ResourceHandler resourceHandler, NotebookSection parentNotebookSection) {
        this.resourceHandler = resourceHandler;
        this.parentNotebookSection = parentNotebookSection;
        Log.info("New notebook initialized with parent notebook " + parentNotebookSection.getUUID(), CLASS_NAME);
        pageLayers = new ArrayList<>();
        init();
    }

    private void init() {
        uuid = UUID.randomUUID();
    }

    public String getName() {
        return name;
    }

    public UUID getUUID() {
        return uuid;
    }

    public void setUUID(String uuidString) {
        uuid = UUID.fromString(uuidString);
    }

    public ArrayList<NotebookPageLayer> getPageLayers() {
        return pageLayers;
    }

    public NotebookPageLayer getPageLayerByUUID(UUID uuid) {
        for (NotebookPageLayer pageLayer : pageLayers) {
            if (uuid.compareTo(pageLayer.getUUID()) == 0) {
                return pageLayer;
            }
        }
        return null;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEditorPane(AnchorPane editorPane) {
        this.editorPane = editorPane;
    }

    public AnchorPane getEditorPane() {
        return editorPane;
    }

    public NotebookSection getParentNotebookSection() {
        return parentNotebookSection;
    }

    public void addPageLayer(NotebookPageLayer newPageLayer) {
        pageLayers.add(newPageLayer);
    }

    public void newPageLayer(Point coords, Point size) {
        NotebookPageLayer pageLayer = new NotebookPageLayer(resourceHandler, coords, size, this);
        addPageLayer(pageLayer);
    }

    public void newPageLayer(Point coords, Point size, String uuid) {
        NotebookPageLayer pageLayer = new NotebookPageLayer(resourceHandler, coords, size, uuid, this);
        addPageLayer(pageLayer);
    }

    public void removePageLayer(NotebookPageLayer pageLayer) {
        try {
            resourceHandler.getFileHandler().removePageLayer(pageLayer);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
