package com.mitchellaugustin.notebook.layout;

import com.mitchellaugustin.notebook.utils.DraggableNodeHandler;
import com.mitchellaugustin.notebook.utils.Log;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

/**
 * Structural class for notebook page layers containing all metadata
 *
 * @author Mitchell Augustin
 */
public class NotebookPageLayer extends Pane {
    public static final String CLASS_NAME = "NotebookPageLayer";

    private NotebookPage parentNotebookPage;
    private ResourceHandler resourceHandler;
    private Point coordinates;
    private Point size;
    private UUID uuid;
    private TextBox textBox;

    /**
     * Instantiates a new NotebookPageLayer object with the provided ResourceHandler metadata
     *
     * @param resourceHandler    - The ResourceHandler object to use
     * @param coords             - The coordinates of the layer
     * @param size               - The layer's size
     * @param parentNotebookPage - The parent NotebookPage object
     */
    public NotebookPageLayer(ResourceHandler resourceHandler, Point coords, Point size, NotebookPage parentNotebookPage) {
        this(resourceHandler, coords, size, UUID.randomUUID().toString(), parentNotebookPage);
    }

    /**
     * Instantiates a new NotebookPageLayer object with the provided ResourceHandler metadata
     * This constructor should be used if a UUID is being provided
     *
     * @param resourceHandler    - The ResourceHandler object to use
     * @param coords             - The coordinates of the layer
     * @param size               - The layer's size
     * @param uuidString         - The UUID string of the layer
     * @param parentNotebookPage - The parent NotebookPage object
     */
    public NotebookPageLayer(ResourceHandler resourceHandler, Point coords, Point size, String uuidString, NotebookPage parentNotebookPage) {
        this.resourceHandler = resourceHandler;
        this.parentNotebookPage = parentNotebookPage;
        this.size = size;
        uuid = UUID.fromString(uuidString);
        coordinates = coords;
        Log.info("NotebookPageLayer (id " + uuid.toString() + ") initialized at location " + coordinates.getX() + ", " + coordinates.getY(), CLASS_NAME);
    }

    public void setData(String textData, Stage stage, Pane editorPane, DraggableNodeHandler dragHandler) throws IOException {
        int indexInEditorPane = editorPane.getChildren().size();
        textBox = new TextBox(resourceHandler, this, textData, stage, editorPane, dragHandler, indexInEditorPane);
        getChildren().add(textBox.get());
    }

    public void setCoords(double newX, double newY) {
        coordinates.setLocation(newX, newY);
    }

    public Point getCoords() {
        return coordinates;
    }

    public Point getSize() {
        return size;
    }

    public TextBox getTextBox() {
        return textBox;
    }

    public UUID getUUID() {
        return uuid;
    }

    public NotebookPage getParentNotebookPage() {
        return parentNotebookPage;
    }

    public void setUUID(String uuidString) {
        uuid = UUID.fromString(uuidString);
    }
}
