package com.mitchellaugustin.notebook.layout;

import com.mitchellaugustin.notebook.utils.Log;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Structural class for notebook sections containing all metadata and child pages
 *
 * @author Mitchell Augustin
 */
public class NotebookSection {
    public static final String CLASS_NAME = "NotebookTab";
    private static int pagesInSection = 0;

    private Notebook parentNotebook;
    private ResourceHandler resourceHandler;
    private String name;
    private UUID uuid;
    private ArrayList<NotebookPage> pages;

    /**
     * Instantiates a new NotebookSection object with the provided ResourceHandler and parent notebook
     *
     * @param resourceHandler - The ResourceHandler object to use
     * @param parentNotebook  - The section's parent Notebook
     */
    public NotebookSection(ResourceHandler resourceHandler, Notebook parentNotebook) {
        Log.info("New notebook initialized with parent notebook " + parentNotebook.getUUID(), CLASS_NAME);
        pages = new ArrayList<>();
        this.resourceHandler = resourceHandler;
        this.parentNotebook = parentNotebook;
        init();
    }

    private void init() {
        uuid = UUID.randomUUID();
    }

    public String getName() {
        return name;
    }

    public UUID getUUID() {
        return uuid;
    }

    public void setUUID(String uuidString) {
        uuid = UUID.fromString(uuidString);
    }

    public ArrayList<NotebookPage> getPages() {
        return pages;
    }

    public Notebook getParentNotebook() {
        return parentNotebook;
    }

    public NotebookPage getPageByUUID(UUID uuid) {
        for (NotebookPage page : pages) {
            if (uuid.compareTo(page.getUUID()) == 0) {
                return page;
            }
        }
        return null;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void removePage(UUID uuid) {
        for (int i = 0; i < pages.size(); i++) {
            if (pages.get(i).getUUID().compareTo(uuid) == 0) {
                //Remove the page's entry from sectionManifest.json
                try {
                    resourceHandler.getFileHandler().removePage(pages.get(i));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                pages.remove(pages.get(i));
            }
        }
        pagesInSection--;
    }

    /**
     * Adds the given NotebookPage to the NotebookSection and its FXML UI tab list.
     */
    public void newPage() {
        NotebookPage newPage = new NotebookPage(resourceHandler, this);
        newPage.setName("New Page " + pagesInSection);
        pages.add(newPage);
        resourceHandler.getNotebookFXMLController().addPage(newPage);
        pagesInSection++;
    }
}
