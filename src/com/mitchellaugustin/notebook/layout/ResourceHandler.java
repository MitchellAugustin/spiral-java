package com.mitchellaugustin.notebook.layout;

import com.mitchellaugustin.notebook.utils.Log;
import javafx.collections.ObservableList;
import javafx.scene.control.MenuBar;
import javafx.stage.Stage;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import java.io.File;
import java.util.ArrayList;

public class ResourceHandler {
    //Window-specific variables
    //This variable is window-specific since the same FXMLLayout will always be the same regardless of which notebook is open.
    private NotebookFXMLController notebookFXMLController;
    private MenuBar menuBar;
    private Stage stage;

    //Notebook-specific variables
    private ArrayList<Notebook> notebooks;

    //Session-specific variables
    private FileHandler fileHandler;
    private Notebook currentNotebook;
    private NotebookSection currentSection;
    private NotebookPage currentPage;

    public ResourceHandler(Stage stage) {
        fileHandler = new FileHandler(this);
        notebooks = new ArrayList<Notebook>();
        this.stage = stage;
    }

    public void openNotebook(Notebook notebook) {
        if (!notebooks.contains(notebook)) {
            notebooks.add(notebook);
        }
        notebookFXMLController.openNotebook(notebook);
        currentNotebook = notebook;
        currentSection = notebook.getSections().get(0);
        currentPage = notebook.getSections().get(0).getPages().get(0);
    }

    public void setCurrentNotebook(Notebook notebook, boolean automatic) throws ParseException {
        currentNotebook = notebook;
        if (notebook.getSections().size() == 0 && automatic) {
            getFileHandler().newSectionObject(notebook.getPath(), this, "New Section", notebook);
            setCurrentSection(notebook.getSections().get(0), notebook, automatic);
        }
        if (notebook.getSections().size() > 0 && automatic) {
            notebookFXMLController.openNotebook(notebook);
            openNotebook(notebook);
        }
    }

    public void setCurrentSection(NotebookSection section, Notebook notebook, boolean automatic) throws ParseException {
        currentNotebook = notebook;
        currentSection = section;
        if (automatic) {
            if (section.getPages().size() == 0) {
                Log.info("currentNotebook.getPath() is " + currentNotebook.getPath());
                getFileHandler().newPageObject(currentNotebook.getPath() + File.separator + "section-" + section.getUUID().toString(), this, "New Page", currentNotebook, section);
            }
            setCurrentPage(section.getPages().get(0));
        }
        notebookFXMLController.openSection(section);
    }

    public void setCurrentPage(NotebookPage page) {
        currentPage = page;
        stage.setTitle(page.getName() + " - Spiral");
    }

    public void renameNotebookDialog(Notebook notebook) throws ParseException {
        String notebookName = JOptionPane.showInputDialog("Enter a name for your notebook:");
        if (notebookName != null && !notebookName.isEmpty()) {
            notebook.setName(notebookName);
            fileHandler.renameNotebook(notebook, notebookName);
            notebookFXMLController.getNotebooksList().getItems().set(notebookFXMLController.getNotebooksList().getSelectionModel().getSelectedIndex(), notebookName);
        }
    }

    public void renameSectionDialog(NotebookSection section) throws ParseException {
        String sectionName = JOptionPane.showInputDialog("Enter a name for your section:");
        if (sectionName != null && !sectionName.isEmpty()) {
            section.setName(sectionName);
            fileHandler.renameSection(sectionName);
            notebookFXMLController.getSectionsList().getItems().set(notebookFXMLController.getSectionsList().getSelectionModel().getSelectedIndex(), sectionName);
        }
    }

    public void renamePageDialog(NotebookPage page) throws ParseException {
        String pageName = JOptionPane.showInputDialog("Enter a name for your page:");
        if (pageName != null && !pageName.isEmpty()) {
            page.setName(pageName);
            fileHandler.renamePage(pageName);
            notebookFXMLController.getPagePane().getTabs().get(notebookFXMLController.getPagePane().getSelectionModel().getSelectedIndex()).setText(pageName);
        }
    }

    /**
     * Adds the given Notebook to the UI list.
     * This method is not included in openNotebook since the Notebook should be visible in the list even if it is closed
     * as long as it is present in the existing Session (session system is not yet implemented, though.)
     *
     * @param notebook
     */
    public void addNotebookToList(Notebook notebook) {
        notebookFXMLController.addNotebookToList(notebook);
    }

    /**
     * Adds the given NotebookSection to the UI list.
     *
     * @param notebookSection
     */
    public void addSectionToList(NotebookSection notebookSection) {
        notebookFXMLController.addSectionToList(notebookSection);
    }

    public ArrayList<Notebook> getNotebooks() {
        return notebooks;
    }

    public Notebook getCurrentNotebook() {
        return currentNotebook;
    }

    public NotebookSection getCurrentNotebookSection() {
        return currentSection;
    }

    public NotebookPage getCurrentNotebookPage() {
        return currentPage;
    }

    public void setNotebookFXMLController(NotebookFXMLController c) {
        notebookFXMLController = c;
    }

    public NotebookFXMLController getNotebookFXMLController() {
        return notebookFXMLController;
    }

    public FileHandler getFileHandler() {
        return fileHandler;
    }

    public void setMenuBar(MenuBar m) {
        menuBar = m;
    }

    public MenuBar getMenuBar() {
        return menuBar;
    }

    public void refreshStylesheet() {
        ObservableList<String> stylesheets = stage.getScene().getStylesheets();
        stylesheets.set(stylesheets.size() - 1, "file:styles.css");
        stage.setScene(stage.getScene());
    }
}