package com.mitchellaugustin.notebook.layout;

import com.mitchellaugustin.notebook.utils.DraggableNodeHandler;
import com.mitchellaugustin.notebook.utils.FileManager;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TextBox {
    public static final String CLASS_NAME = "TextBox";

    private ResourceHandler resourceHandler;
    private NotebookPageLayer parentNotebookPageLayer;

    Node thisNode;
    private FileManager fileManager;
    private TextBoxFXMLController controller;
    private Pane editorPane;
    private DraggableNodeHandler dragHandler;

    public TextBox(ResourceHandler resourceHandler, NotebookPageLayer parentNotebookPageLayer, String text, Stage stage, Pane editorPane, DraggableNodeHandler dragHandler, int indexInEditorPane) throws IOException {
        this.resourceHandler = resourceHandler;
        this.parentNotebookPageLayer = parentNotebookPageLayer;
        this.editorPane = editorPane;
        this.dragHandler = dragHandler;

        fileManager = new FileManager();

        thisNode = configureFXMLLayout(stage);

        controller.setText(text);
    }


    /**
     * Configures the notebook and tab selector sidebar with the currently opened notebooks
     * Note: A list of currently open notebooks will be pulled from a file in the app's directory,
     * so nothing other than the Group and Stage need to be passed as parameters to this method.
     *
     * @throws IOException
     * @throws FileNotFoundException
     */
    public Parent configureFXMLLayout(Stage primaryStage) throws FileNotFoundException, IOException {
        FXMLLoader loader = new FXMLLoader();
        controller = new TextBoxFXMLController(resourceHandler, parentNotebookPageLayer, primaryStage, editorPane, dragHandler);


        loader.setController(controller);
        Parent fxmlLayout = loader.load(new FileInputStream(fileManager.getAppDirectory() + File.separator + "textbox.fxml"));

        return fxmlLayout;
    }

    public Node get() {
        return thisNode;
    }

    public TextBoxFXMLController getController() {
        return controller;
    }
}
