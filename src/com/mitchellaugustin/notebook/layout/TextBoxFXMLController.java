package com.mitchellaugustin.notebook.layout;

import com.mitchellaugustin.notebook.utils.*;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.MenuBar;
import javafx.scene.image.Image;
import javafx.scene.input.Clipboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import netscape.javascript.JSException;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.util.UUID;

public class TextBoxFXMLController {
    public static final String CLASS_NAME = "TextBoxFXMLController";

    ResourceHandler resourceHandler;
    NotebookPageLayer parentNotebookPageLayer;
    Stage stage;
    DraggableNodeHandler dragHandler;
    Pane editorPane;

    public TextBoxFXMLController(ResourceHandler resourceHandler, NotebookPageLayer parentNotebookPageLayer, Stage primaryStage, Pane editorPane, DraggableNodeHandler dragHandler) {
        this.resourceHandler = resourceHandler;
        this.parentNotebookPageLayer = parentNotebookPageLayer;
        stage = primaryStage;
        this.editorPane = editorPane;
        this.dragHandler = dragHandler;
    }

    @FXML
    private MenuBar menuBar;

    @FXML
    private HTMLEditor htmlEditor;

    @FXML
    private VBox vbox;

    @FXML
    private void initialize() {
        menuBar.getMenus().remove(0, 3);
        menuBar.setVisible(true);
        menuBar.setMinHeight(10);
        menuBar.setOnMousePressed(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                Log.info("MouseEvent detected on MenuBar", CLASS_NAME);
                editorPane.getChildren().add(dragHandler.makeDraggable());
            }

        });

        //Note: Since all MouseEvents are consumed by the dragHandler once dragging is initialized, onMouseReleased is handled in the DraggableNodeHandler class.

        setToolbarVisibility(false);

        setBackgroundColor();
        htmlEditor.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setToolbarVisibility(true);
            }
        });

        htmlEditor.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                checkHeight();
                if (HTMLEditorUtils.isEmpty(htmlEditor)) {
                    resourceHandler.getNotebookFXMLController().removePageLayer(dragHandler.isDraggable() ? dragHandler.makeDraggable() : dragHandler.makeUndraggable());
                    resourceHandler.getCurrentNotebookPage().removePageLayer(parentNotebookPageLayer);
                }
            }
        });

        htmlEditor.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                setToolbarVisibility(true);
                try {
                    resourceHandler.getFileHandler().updatePageLayer(parentNotebookPageLayer);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

//		Button button = (Button) htmlEditor.lookup(".html-editor-paste");
//		button.addEventFilter(MouseEvent.MOUSE_PRESSED, e -> {
//			System.out.println("paste pressed");
//			pasteImage();
//		});

        htmlEditor.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
            if (e.isControlDown() && e.getCode() == KeyCode.V && Clipboard.getSystemClipboard().hasImage() && !Clipboard.getSystemClipboard().hasString()) {
                System.out.println("CTRL+V pressed");
                pasteImage();
            }
        });

        DragResizer.makeResizable(htmlEditor, htmlEditor, new Runnable() {
            @Override
            public void run() {
                Log.info("Layer resized, updating pageManifest");
                try {
                    resourceHandler.getFileHandler().updatePageLayer(parentNotebookPageLayer);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private int lastOffsetHeight;

    private void setBackgroundColor() {
        WebEngine engine = ((WebView) htmlEditor.lookup(".web-view")).getEngine();
//		final WebPage webPage = com.sun.javafx.webkit.Accessor.getPageFor(engine);
//		webPage.setBackgroundColor((new java.awt.Color(0, 0, 0, 0)).getRGB());
    }

    private void pasteImage() {
        Clipboard clipboard = Clipboard.getSystemClipboard();
        Image clipboardImage = clipboard.getImage();
        String imagePath = resourceHandler.getCurrentNotebook().getPath() + File.separator + "section-" + resourceHandler.getCurrentNotebookSection().getUUID().toString() + File.separator + "page-" + resourceHandler.getCurrentNotebookPage().getUUID().toString() + File.separator + UUID.randomUUID().toString() + ".png";
        SaveFile.saveImageToFile(imagePath, clipboardImage);
        HTMLEditorUtils.addImage(htmlEditor, imagePath);
    }

    private void checkHeight() {
        try {
            HTMLEditorUtils.wrapInDiv(htmlEditor);
            WebView webView = (WebView) htmlEditor.lookup(".web-view");

            int newHeight = (Integer) webView.getEngine().executeScript("document.getElementById(\"measured\").offsetHeight;") + 28;

            if (newHeight != lastOffsetHeight) {
                lastOffsetHeight = newHeight;
                webView.setPrefHeight(newHeight);
            }

            Log.info("Layer resized, updating pageManifest");
            try {
                resourceHandler.getFileHandler().updatePageLayer(parentNotebookPageLayer);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (JSException e) {
            Log.warn("Unable to auto-resize. Please ensure the htmlEditor content is wrapped in a div with id \"measured\".", CLASS_NAME);
        }
    }
    //Note: Can also use @FXML items instead of object.setWhatever methods

    public void setToolbarVisibility(boolean visible) {
        if (visible) {
            for (NotebookPageLayer layer : resourceHandler.getCurrentNotebookPage().getPageLayers()) {
                layer.getTextBox().getController().setToolbarVisibility(false);
            }
        }
        HTMLEditorUtils.setToolbarVisibility(htmlEditor, visible);
    }

    protected void setDragHandler(DraggableNodeHandler dragHandler) {
        this.dragHandler = dragHandler;
    }

    public void setText(String text) {
        htmlEditor.setHtmlText(text);
    }

    public String getText() {
        return htmlEditor.getHtmlText();
    }

    public HTMLEditor getHtmlEditor() {
        return htmlEditor;
    }
}