package com.mitchellaugustin.notebook.layout;

import com.mitchellaugustin.notebook.toolbars.MenuBarSetup;
import com.mitchellaugustin.notebook.utils.FileManager;
import com.mitchellaugustin.notebook.utils.URLHandler;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

/**
 * Main class and program entry point used for high-level handling of all Windows and staging.
 *
 * @author Mitchell Augustin
 * @version 11-4-2019
 */
public class Window extends Application {
    private FileManager fileManager;
    private VBox rootLayout;
    private NotebookFXMLController notebookFXMLController;
    private ResourceHandler resourceHandler;

    @Override
    public void start(Stage stage) throws Exception {
        Group rootGroup = new Group();
        fileManager = new FileManager();
        resourceHandler = new ResourceHandler(stage);
        initializeNotebookView(rootGroup, stage, resourceHandler);

        //Locates the session file in the user's Spiral installation directory and opens all of the included notebooks
        resourceHandler.getFileHandler().openNotebooksFromSessionFile();

        //Sets a custom URL handler so clicked links open in the host browser instead of the HTMLEditor
        URL.setURLStreamHandlerFactory(protocol -> {

            if (protocol.startsWith("http")) {
                return new URLHandler(this);
            }

            return null;
        });
        //Note: Use this to set a custom stylesheet depending on the OS. May use in the future if users want custom skins.
//		setUserAgentStylesheet(STYLESHEET_MODENA);
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Retrieves the contents of the specified Notebook and displays them to the Window.
     *
     * @param root
     * @param stage
     * @throws IOException
     * @throws FileNotFoundException
     */
    private void initializeNotebookView(Group root, Stage stage, ResourceHandler resourceHandler) throws FileNotFoundException, IOException {
        configureViewport(root, stage, resourceHandler);
        stage.setTitle("Notebook");
        stage.getIcons().add(new Image("file:logo.png"));
        stage.show();
    }

    /**
     * General method that configures the viewport to prepare it for the notebook
     *
     * @param root
     * @param stage
     * @throws IOException
     * @throws FileNotFoundException
     */
    private void configureViewport(Group root, Stage stage, ResourceHandler resourceHandler) throws FileNotFoundException, IOException {
        MenuBar menuBar = configureMenuBar();
        resourceHandler.setMenuBar(menuBar);
        Parent fxmlLayout = configureFXMLLayout(stage);

        rootLayout = new VBox(menuBar, fxmlLayout);


        stage.setMaximized(true);
        rootLayout.setPrefSize(1280, 720);
        rootLayout.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        VBox.setVgrow(fxmlLayout, Priority.ALWAYS);
        Scene scene = new Scene(rootLayout);
        scene.getStylesheets().add("file:styles.css");
        scene.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.isAltDown()) {
                    event.consume();
                }
            }
        });
        stage.setScene(scene);
    }

    private MenuBar configureMenuBar() throws IOException {
        MenuBarSetup menuBarSetup = new MenuBarSetup();
        MenuBar menuBar = menuBarSetup.getMenuBar(resourceHandler);

        return menuBar;
    }

    /**
     * Configures the notebook and tab selector sidebar with the currently opened notebooks
     * Note: A list of currently open notebooks will be pulled from a file in the app's directory,
     * so nothing other than the Group and Stage need to be passed as parameters to this method.
     *
     * @throws IOException
     * @throws FileNotFoundException
     */
    private Parent configureFXMLLayout(Stage primaryStage) throws FileNotFoundException, IOException {
        FXMLLoader loader = new FXMLLoader();

        notebookFXMLController = new NotebookFXMLController(primaryStage, resourceHandler);

        loader.setController(notebookFXMLController);
        resourceHandler.setNotebookFXMLController(notebookFXMLController);

        Parent fxmlLayout = loader.load(new FileInputStream(fileManager.getAppDirectory() + File.separator + "layout.fxml"));

        return fxmlLayout;
    }
}
