package com.mitchellaugustin.notebook.toolbars;

import com.mitchellaugustin.notebook.layout.NotebookPage;
import com.mitchellaugustin.notebook.layout.NotebookPageLayer;
import com.mitchellaugustin.notebook.layout.NotebookSection;
import com.mitchellaugustin.notebook.layout.ResourceHandler;
import com.mitchellaugustin.notebook.utils.Constants;
import com.mitchellaugustin.notebook.utils.Log;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import org.json.simple.parser.ParseException;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;


/**
 * This class will handle all toolbar positioning/configuration.
 * Each toolbar will have its own class within the "toolbars" package similar to this one.
 * @author Mitchell Augustin
 *
 */
public class MenuBarSetup {
	MenuBar menuBar;
	

	/**
	 * Configures the primary MenuBar (file, edit, etc.) and returns it.
	 */
	public MenuBar getMenuBar(ResourceHandler resourceHandler) {
		setupMenuBar(resourceHandler);
		
		return menuBar;
	}
	
	private void setupMenuBar(ResourceHandler resourceHandler) {
		//Configures the MenuBar
		Menu fileMenu = new Menu("_File");
		Menu newFileMenu = new Menu("_New");
		newFileMenu.setAccelerator(KeyCombination.keyCombination("SHORTCUT+N"));
		
		//"New File" submenus begin here
				MenuItem pageMenuItem = new MenuItem("_Page");
				pageMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+N"));
				pageMenuItem.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						Log.info("page button pressed");
						try {
							resourceHandler.getFileHandler().newPageDialog(resourceHandler.getCurrentNotebook(), resourceHandler.getCurrentNotebookSection());
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}
				});
		
				newFileMenu.getItems().add(pageMenuItem);
		
				MenuItem sectionMenuItem = new MenuItem("_Section");
				sectionMenuItem.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						Log.info("section button pressed");
//						resourceHandler.getCurrentNotebook().newSection();
						try {
							resourceHandler.getFileHandler().newSectionDialog(resourceHandler.getCurrentNotebook());
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}
				});
		
				newFileMenu.getItems().add(sectionMenuItem);
		
				MenuItem notebookMenuItem = new MenuItem("_Notebook");
				notebookMenuItem.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						Log.info("notebook button pressed");
						try {
							resourceHandler.getFileHandler().newNotebookDialog();
						} catch (ParseException e) {
							e.printStackTrace();
						}
					}
				});
		
				newFileMenu.getItems().add(notebookMenuItem);

				MenuItem explainMenuItem = new MenuItem("_Explain Choices on this Menu");
				explainMenuItem.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						Log.info("explain button pressed");
						try {
							Desktop.getDesktop().browse(new URI(Constants.EXPLAIN_CHOICES_URL));
						} catch (IOException e) {
							e.printStackTrace();
						} catch (URISyntaxException e) {
							e.printStackTrace();
						}
					}
				});
		
				newFileMenu.getItems().add(explainMenuItem);

		//"New File" submenus end here
		
		fileMenu.getItems().add(newFileMenu);
		
		MenuItem openMenuItem = new MenuItem("_Open");
		openMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+O"));
		openMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("open button pressed");
				try {
					resourceHandler.getFileHandler().openNotebookDialog();
				} catch (ParseException | IOException e) {
					e.printStackTrace();
				}
			}
		});

		fileMenu.getItems().add(openMenuItem);

		MenuItem closeNotebookMenuItem = new MenuItem("C_lose this Notebook");
		closeNotebookMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("closeNotebook button pressed");
				resourceHandler.getNotebookFXMLController().removeNotebook(resourceHandler.getNotebookFXMLController().getNotebooksList().getSelectionModel().getSelectedIndex(), resourceHandler.getCurrentNotebook());
			}
		});

		fileMenu.getItems().add(closeNotebookMenuItem);

		MenuItem publishMenuItem = new MenuItem("Pu_blish as PDF or XPS");
		publishMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("publish button pressed");
			}
		});

//		fileMenu.getItems().add(publishMenuItem);

		MenuItem sendToMenuItem = new MenuItem("Sen_d To");
		sendToMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("sendTo button pressed");
			}
		});

//		fileMenu.getItems().add(sendToMenuItem);

		MenuItem pageSetupMenuItem = new MenuItem("Page Set_up");
		pageSetupMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("pageSetup button pressed");
			}
		});

//		fileMenu.getItems().add(pageSetupMenuItem);

		MenuItem printPreviewMenuItem = new MenuItem("Print Pre_view");
		printPreviewMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("printPreview button pressed");
			}
		});

//		fileMenu.getItems().add(printPreviewMenuItem);

		MenuItem printMenuItem = new MenuItem("_Print");
		printMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+P"));
		printMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("print button pressed");
			}
		});

//		fileMenu.getItems().add(printMenuItem);

		MenuItem notebookPropertiesMenuItem = new MenuItem("Notebook Propert_ies");
		notebookPropertiesMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("notebookProperties button pressed");
				JOptionPane.showMessageDialog(null,
				"Name: " + resourceHandler.getCurrentNotebook().getName()
				+ "\nPath: " + resourceHandler.getCurrentNotebook().getPath()
				+ "\nTotal sections: " + resourceHandler.getCurrentNotebook().getSections().size(), "Notebook Information", JOptionPane.INFORMATION_MESSAGE);
			}
		});

		fileMenu.getItems().add(notebookPropertiesMenuItem);

		MenuItem exitMenuItem = new MenuItem("E_xit");
		exitMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("exit button pressed");
				System.exit(0);
			}
		});

		fileMenu.getItems().add(exitMenuItem);

		
		//BEGIN EDIT MENU
		Menu editMenu = new Menu("_Edit");
		
		
		MenuItem undoMenuItem = new MenuItem("_Undo Typing");
		undoMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+Z"));
		undoMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("undo button pressed");
			}
		});

		editMenu.getItems().add(undoMenuItem);

		MenuItem redoMenuItem = new MenuItem("_Redo");
		redoMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+Y"));
		redoMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("redo button pressed");
			}
		});

		editMenu.getItems().add(redoMenuItem);

		MenuItem cutMenuItem = new MenuItem("Cu_t");
		cutMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+X"));
		cutMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("cut button pressed");
			}
		});

		editMenu.getItems().add(cutMenuItem);

		MenuItem copyMenuItem = new MenuItem("_Copy");
		copyMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+C"));
		copyMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("copy button pressed");
			}
		});

		editMenu.getItems().add(copyMenuItem);

		MenuItem copyHyperlinkMenuItem = new MenuItem("Copy _Hyperlink To");
		copyHyperlinkMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("copyHyperlink button pressed");
			}
		});

		editMenu.getItems().add(copyHyperlinkMenuItem);

		MenuItem pasteMenuItem = new MenuItem("_Paste");
		pasteMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+V"));
		pasteMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("paste button pressed");
			}
		});

		editMenu.getItems().add(pasteMenuItem);

		MenuItem deleteMenuItem = new MenuItem("_Delete");
		deleteMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("delete button pressed");
			}
		});

		editMenu.getItems().add(deleteMenuItem);

		MenuItem orderMenuItem = new MenuItem("_Order");
		orderMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("order button pressed");
			}
		});

		editMenu.getItems().add(orderMenuItem);

		MenuItem selectMenuItem = new MenuItem("_Select");
		selectMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("select button pressed");
			}
		});

		editMenu.getItems().add(selectMenuItem);

		MenuItem snapToGridMenuItem = new MenuItem("Sn_ap To Grid");
		snapToGridMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("snapToGrid button pressed");
			}
		});

		editMenu.getItems().add(snapToGridMenuItem);

		MenuItem findMenuItem = new MenuItem("_Find");
		findMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+F"));
		findMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("find button pressed");
			}
		});

		editMenu.getItems().add(findMenuItem);


		//BEGIN VIEW MENU
		
		Menu viewMenu = new Menu("_View");
		MenuItem pagesSortedByDateMenuItem = new MenuItem("Pages _Sorted By Date");
		pagesSortedByDateMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("pagesSortedByDate button pressed");
			}
		});

		viewMenu.getItems().add(pagesSortedByDateMenuItem);

		MenuItem pagesNotReadMenuItem = new MenuItem("Pages I Haven't _Read");
		pagesNotReadMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("pagesNotRead button pressed");
			}
		});

		viewMenu.getItems().add(pagesNotReadMenuItem);

		MenuItem pagesChangedRecentlyMenuItem = new MenuItem("Pages _Changed Recently");
		pagesChangedRecentlyMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("pagesChangedRecently button pressed");
			}
		});

		viewMenu.getItems().add(pagesChangedRecentlyMenuItem);

		MenuItem allTaggedNotesMenuItem = new MenuItem("_All Tagged Notes");
		allTaggedNotesMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("allTaggedNotes button pressed");
			}
		});

		viewMenu.getItems().add(allTaggedNotesMenuItem);

		MenuItem fullPageViewMenuItem = new MenuItem("Full _Page View");
		fullPageViewMenuItem.setAccelerator(KeyCombination.keyCombination("F11"));
		fullPageViewMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("fullPageView button pressed");
			}
		});

		viewMenu.getItems().add(fullPageViewMenuItem);

		MenuItem drawingToolbarMenuItem = new MenuItem("_Drawing Toolbar");
		drawingToolbarMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("drawingToolbar button pressed");
			}
		});

		viewMenu.getItems().add(drawingToolbarMenuItem);

		MenuItem tagsToolbarMenuItem = new MenuItem("Ta_gs Toolbar");
		tagsToolbarMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("tagsToolbar button pressed");
			}
		});

		viewMenu.getItems().add(tagsToolbarMenuItem);

		MenuItem myPensToolbarMenuItem = new MenuItem("My P_ens Toolbar");
		myPensToolbarMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("myPensToolbar button pressed");
			}
		});

		viewMenu.getItems().add(myPensToolbarMenuItem);

		MenuItem audioVideoToolbarMenuItem = new MenuItem("Audio and _Video Toolbar");
		audioVideoToolbarMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("audioVideoToolbar button pressed");
			}
		});

		viewMenu.getItems().add(audioVideoToolbarMenuItem);

		MenuItem toolbarsMenuItem = new MenuItem("_Toolbars");
		toolbarsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("toolbars button pressed");
			}
		});

		viewMenu.getItems().add(toolbarsMenuItem);

		MenuItem taskPaneMenuItem = new MenuItem("Tas_k Pane");
		taskPaneMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+F1"));
		taskPaneMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("taskPane button pressed");
			}
		});

		viewMenu.getItems().add(taskPaneMenuItem);

		
		//BEGIN INSERT MENU
		Menu insertMenu = new Menu("_Insert");
		
		MenuItem tagMenuItem = new MenuItem("T_ag");
		tagMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("tag button pressed");
			}
		});

		insertMenu.getItems().add(tagMenuItem);

		MenuItem picturesMenuItem = new MenuItem("_Pictures");
		picturesMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("pictures button pressed");
			}
		});

		insertMenu.getItems().add(picturesMenuItem);

		MenuItem filesMenuItem = new MenuItem("_Files");
		filesMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("files button pressed");
			}
		});

		insertMenu.getItems().add(filesMenuItem);

		MenuItem filesAsPrintoutsMenuItem = new MenuItem("Files as Print_outs");
		filesAsPrintoutsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("filesAsPrintouts button pressed");
			}
		});

		insertMenu.getItems().add(filesAsPrintoutsMenuItem);

		MenuItem screenClippingMenuItem = new MenuItem("Sc_reen Clipping");
		screenClippingMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("screenClipping button pressed");
			}
		});

		insertMenu.getItems().add(screenClippingMenuItem);

		MenuItem audioRecordingMenuItem = new MenuItem("A_udio Recording");
		audioRecordingMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("audioRecording button pressed");
			}
		});

		insertMenu.getItems().add(audioRecordingMenuItem);

		MenuItem videoRecordingMenuItem = new MenuItem("_Video Recording");
		videoRecordingMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("videoRecording button pressed");
			}
		});

		insertMenu.getItems().add(videoRecordingMenuItem);

		MenuItem symbolMenuItem = new MenuItem("_Symbol");
		symbolMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("symbol button pressed");
			}
		});

		insertMenu.getItems().add(symbolMenuItem);

		MenuItem dateAndTimeMenuItem = new MenuItem("Date and _Time");
		dateAndTimeMenuItem.setAccelerator(KeyCombination.keyCombination("ALT+SHIFT+F"));
		dateAndTimeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("dateAndTime button pressed");
			}
		});

		insertMenu.getItems().add(dateAndTimeMenuItem);

		MenuItem hyperlinkMenuItem = new MenuItem("Hyperl_ink");
		hyperlinkMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+K"));
		hyperlinkMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("hyperlink button pressed");
			}
		});

		insertMenu.getItems().add(hyperlinkMenuItem);

		MenuItem extraWritingSpaceMenuItem = new MenuItem("Extra Writi_ng Space");
		extraWritingSpaceMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("extraWritingSpace button pressed");
			}
		});

		insertMenu.getItems().add(extraWritingSpaceMenuItem);


		//BEGIN FORMAT MENU
		Menu formatMenu = new Menu("F_ormat");
		
		MenuItem fontMenuItem = new MenuItem("_Font");
		fontMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+D"));
		fontMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("font button pressed");
			}
		});

		formatMenu.getItems().add(fontMenuItem);

		MenuItem bulletsMenuItem = new MenuItem("_Bullets");
		bulletsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("bullets button pressed");
			}
		});

		formatMenu.getItems().add(bulletsMenuItem);

		MenuItem numberingMenuItem = new MenuItem("_Numbering");
		numberingMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("numbering button pressed");
			}
		});

		formatMenu.getItems().add(numberingMenuItem);

		MenuItem listMenuItem = new MenuItem("_List");
		listMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("list button pressed");
			}
		});

		formatMenu.getItems().add(listMenuItem);

		MenuItem ruleLinesMenuItem = new MenuItem("_Rule Lines");
		ruleLinesMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("ruleLines button pressed");
			}
		});

		formatMenu.getItems().add(ruleLinesMenuItem);

		MenuItem showPageTitleMenuItem = new MenuItem("_Show Page Title");
		showPageTitleMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("showPageTitle button pressed");
			}
		});

		formatMenu.getItems().add(showPageTitleMenuItem);

		MenuItem templatesMenuItem = new MenuItem("_Templates");
		templatesMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("templates button pressed");
			}
		});

		formatMenu.getItems().add(templatesMenuItem);

		MenuItem sectionColorMenuItem = new MenuItem("Section _Color");
		sectionColorMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("sectionColor button pressed");
			}
		});

		formatMenu.getItems().add(sectionColorMenuItem);

		MenuItem notebookColorMenuItem = new MenuItem("Noteboo_k Color");
		notebookColorMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("notebookColor button pressed");
			}
		});

		formatMenu.getItems().add(notebookColorMenuItem);

		
		//BEGIN SHARE MENU
		Menu shareMenu = new Menu("_Share");
		
		MenuItem createSharedNotebookMenuItem = new MenuItem("_Create Shared Notebook");
		createSharedNotebookMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("createSharedNotebook button pressed");
			}
		});

		shareMenu.getItems().add(createSharedNotebookMenuItem);

		MenuItem sendSharedNotebookLinkMenuItem = new MenuItem("_Send Shared Notebook Link to Others");
		sendSharedNotebookLinkMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("sendSharedNotebookLink button pressed");
			}
		});

		shareMenu.getItems().add(sendSharedNotebookLinkMenuItem);

		MenuItem stopSharingNotebookMenuItem = new MenuItem("S_top Sharing Notebook");
		stopSharingNotebookMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("stopSharingNotebook button pressed");
			}
		});

		shareMenu.getItems().add(stopSharingNotebookMenuItem);

		MenuItem liveSharingSessionMenuItem = new MenuItem("_Live Sharing Session");
		liveSharingSessionMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("liveSharingSession button pressed");
			}
		});

		shareMenu.getItems().add(liveSharingSessionMenuItem);

		MenuItem explainChoicesMenuItem = new MenuItem("_Explain Choices on this Menu");
		explainChoicesMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("explainChoices button pressed");
			}
		});

		shareMenu.getItems().add(explainChoicesMenuItem);


		//BEGIN TOOLS MENU
		Menu toolsMenu = new Menu("_Tools");
		
//		MenuItem spellingMenuItem = new MenuItem("_Spelling");
//		spellingMenuItem.setOnAction(new EventHandler<ActionEvent>() {
//			@Override
//			public void handle(ActionEvent event) {
//				Log.info("spelling button pressed");
//			}
//		});
//
//		toolsMenu.getItems().add(spellingMenuItem);
//
//		MenuItem researchMenuItem = new MenuItem("_Research");
//		researchMenuItem.setOnAction(new EventHandler<ActionEvent>() {
//			@Override
//			public void handle(ActionEvent event) {
//				Log.info("research button pressed");
//			}
//		});
//
//		toolsMenu.getItems().add(researchMenuItem);
//
//		MenuItem setLanguageMenuItem = new MenuItem("Set _Language");
//		setLanguageMenuItem.setOnAction(new EventHandler<ActionEvent>() {
//			@Override
//			public void handle(ActionEvent event) {
//				Log.info("setLanguage button pressed");
//			}
//		});
//
//		toolsMenu.getItems().add(setLanguageMenuItem);
//
//		MenuItem documentManagementMenuItem = new MenuItem("Doc_ument Management");
//		documentManagementMenuItem.setOnAction(new EventHandler<ActionEvent>() {
//			@Override
//			public void handle(ActionEvent event) {
//				Log.info("documentManagement button pressed");
//			}
//		});
//
//		toolsMenu.getItems().add(documentManagementMenuItem);
//
//		MenuItem writingToolsMenuItem = new MenuItem("_Writing Tools");
//		writingToolsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
//			@Override
//			public void handle(ActionEvent event) {
//				Log.info("writingTools button pressed");
//			}
//		});
//
//		toolsMenu.getItems().add(writingToolsMenuItem);
//
//		MenuItem penModeMenuItem = new MenuItem("Pen _Mode");
//		penModeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
//			@Override
//			public void handle(ActionEvent event) {
//				Log.info("penMode button pressed");
//			}
//		});
//
//		toolsMenu.getItems().add(penModeMenuItem);
//
//		MenuItem convertHandwritingToTextMenuItem = new MenuItem("Con_vert Handwriting to Text");
//		convertHandwritingToTextMenuItem.setOnAction(new EventHandler<ActionEvent>() {
//			@Override
//			public void handle(ActionEvent event) {
//				Log.info("convertHandwritingToText button pressed");
//			}
//		});
//
//		toolsMenu.getItems().add(convertHandwritingToTextMenuItem);
//
//		MenuItem treatSelectedInkAsMenuItem = new MenuItem("Treat Selected _Ink As");
//		treatSelectedInkAsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
//			@Override
//			public void handle(ActionEvent event) {
//				Log.info("treatSelectedInkAs button pressed");
//			}
//		});
//
//		toolsMenu.getItems().add(treatSelectedInkAsMenuItem);
//
//		MenuItem autoCorrectOptionsMenuItem = new MenuItem("_AutoCorrect Options");
//		autoCorrectOptionsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
//			@Override
//			public void handle(ActionEvent event) {
//				Log.info("autoCorrectOptions button pressed");
//			}
//		});
//
//		toolsMenu.getItems().add(autoCorrectOptionsMenuItem);
//
//		MenuItem customizeMenuItem = new MenuItem("_Customize");
//		customizeMenuItem.setOnAction(new EventHandler<ActionEvent>() {
//			@Override
//			public void handle(ActionEvent event) {
//				Log.info("customize button pressed");
//			}
//		});
//
//		toolsMenu.getItems().add(customizeMenuItem);
//
//		MenuItem optionsMenuItem = new MenuItem("_Options");
//		optionsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
//			@Override
//			public void handle(ActionEvent event) {
//				Log.info("options button pressed");
//			}
//		});
//
//		toolsMenu.getItems().add(optionsMenuItem);
		
		MenuItem printEditorMenuItem = new MenuItem("_Print content to log");
		printEditorMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+SHIFT+P"));
		printEditorMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("Print HTMLEditor content button pressed");
				for(NotebookPageLayer layer : resourceHandler.getCurrentNotebookPage().getPageLayers()){
					Log.info(layer.getTextBox().getController().getText());
				}

			}
		});

		toolsMenu.getItems().add(printEditorMenuItem);


		MenuItem refreshStylesheetsMenuItem = new MenuItem("_Refresh Stylesheets");
		refreshStylesheetsMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+SHIFT+R"));
		refreshStylesheetsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("Stylesheets refreshed");
				resourceHandler.refreshStylesheet();
			}
		});

		toolsMenu.getItems().add(refreshStylesheetsMenuItem);


		//BEGIN TABLE MENU
		Menu tableMenu = new Menu("Ta_ble");
		MenuItem insertTableMenuItem = new MenuItem("_Insert Table");
		insertTableMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("insertTable button pressed");
			}
		});

		tableMenu.getItems().add(insertTableMenuItem);

		MenuItem insertColumnsToLeftMenuItem = new MenuItem("Insert Columns to the _Left");
		insertColumnsToLeftMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+ALT+E"));
		insertColumnsToLeftMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("insertColumnsToLeft button pressed");
			}
		});

		tableMenu.getItems().add(insertColumnsToLeftMenuItem);

		MenuItem insertColumnsToRightMenuItem = new MenuItem("Insert Columns to the _Right");
		insertColumnsToRightMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+ALT+R"));
		insertColumnsToRightMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("insertColumnsToRight button pressed");
			}
		});

		tableMenu.getItems().add(insertColumnsToRightMenuItem);

		MenuItem insertRowsAboveMenuItem = new MenuItem("Insert Rows _Above");
		insertRowsAboveMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("insertRowsAbove button pressed");
			}
		});

		tableMenu.getItems().add(insertRowsAboveMenuItem);

		MenuItem insertRowsBelowMenuItem = new MenuItem("Insert Rows _Below");
		insertRowsBelowMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+ENTER"));
		insertRowsBelowMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("insertRowsBelow button pressed");
			}
		});

		tableMenu.getItems().add(insertRowsBelowMenuItem);

		MenuItem deleteTableMenuItem = new MenuItem("Delete Tabl_e");
		deleteTableMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("deleteTable button pressed");
			}
		});

		tableMenu.getItems().add(deleteTableMenuItem);

		MenuItem deleteColumnsMenuItem = new MenuItem("Delete Col_umns");
		deleteColumnsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("deleteColumns button pressed");
			}
		});

		tableMenu.getItems().add(deleteColumnsMenuItem);

		MenuItem deleteRowsMenuItem = new MenuItem("Delete Ro_ws");
		deleteRowsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("deleteRows button pressed");
			}
		});

		tableMenu.getItems().add(deleteRowsMenuItem);

		MenuItem selectTableMenuItem = new MenuItem("_Select Table");
		selectTableMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("selectTable button pressed");
			}
		});

		tableMenu.getItems().add(selectTableMenuItem);

		MenuItem selectColumnsMenuItem = new MenuItem("Select Colu_mns");
		selectColumnsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("selectColumns button pressed");
			}
		});

		tableMenu.getItems().add(selectColumnsMenuItem);

		MenuItem selectRowsMenuItem = new MenuItem("Select R_ows");
		selectRowsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("selectRows button pressed");
			}
		});

		tableMenu.getItems().add(selectRowsMenuItem);

		MenuItem selectCellMenuItem = new MenuItem("Select _Cell");
		selectCellMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("selectCell button pressed");
			}
		});

		tableMenu.getItems().add(selectCellMenuItem);

		MenuItem showBordersMenuItem = new MenuItem("S_how Borders");
		showBordersMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("showBorders button pressed");
			}
		});

		tableMenu.getItems().add(showBordersMenuItem);


		//BEGIN WINDOW MENU
		Menu windowMenu = new Menu("_Window");
		
		MenuItem newWindowMenuItem = new MenuItem("_New Window");
		newWindowMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+M"));
		newWindowMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("newWindow button pressed");
			}
		});

		windowMenu.getItems().add(newWindowMenuItem);

		MenuItem newSideNoteWindowMenuItem = new MenuItem("New _Side Note Window");
		newSideNoteWindowMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+SHIFT+M"));
		newSideNoteWindowMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("newSideNoteWindow button pressed");
			}
		});

		windowMenu.getItems().add(newSideNoteWindowMenuItem);

		MenuItem keepWindowOnTopMenuItem = new MenuItem("Keep Window _on Top");
		keepWindowOnTopMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("keepWindowOnTop button pressed");
			}
		});

		windowMenu.getItems().add(keepWindowOnTopMenuItem);


		//BEGIN HELP MENU
		Menu helpMenu = new Menu("_Help");
		MenuItem notebookHelpMenuItem = new MenuItem("Notebook _Help");
		notebookHelpMenuItem.setAccelerator(KeyCombination.keyCombination("F1"));
		notebookHelpMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("notebookHelp button pressed");
				try {
					Desktop.getDesktop().browse(new URI(Constants.HELP_URL));
				} catch (IOException e) {
					e.printStackTrace();
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}
		});

		helpMenu.getItems().add(notebookHelpMenuItem);

		MenuItem keyboardShortcutsMenuItem = new MenuItem("Key_board Shortcuts");
		keyboardShortcutsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("keyboardShortcuts button pressed");
				try {
					Desktop.getDesktop().browse(new URI(Constants.KEYBOARD_SHORTCUTS_URL));
				} catch (IOException e) {
					e.printStackTrace();
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}
		});

		helpMenu.getItems().add(keyboardShortcutsMenuItem);

		MenuItem contactUsMenuItem = new MenuItem("_Contact Us / Contribute");
		contactUsMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("contactUs button pressed");
				try {
					Desktop.getDesktop().browse(new URI(Constants.CONTRIBUTE_URL));
				} catch (IOException e) {
					e.printStackTrace();
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}
		});

		helpMenu.getItems().add(contactUsMenuItem);

		MenuItem checkForUpdatesMenuItem = new MenuItem("Chec_k for Updates");
		checkForUpdatesMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("checkForUpdates button pressed");
				try {
					Desktop.getDesktop().browse(new URI(Constants.UPDATE_CHECK_URL));
				} catch (IOException e) {
					e.printStackTrace();
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}
		});

		helpMenu.getItems().add(checkForUpdatesMenuItem);

		MenuItem aboutNotebookMenuItem = new MenuItem("_About Spiral");
		aboutNotebookMenuItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Log.info("aboutNotebook button pressed");
				JOptionPane.showMessageDialog(null,
						"Spiral version: " + Constants.VERSION + "\n" +
								"Copyright 2019 Mitchell Augustin", "Notebook Information", JOptionPane.INFORMATION_MESSAGE);
			}
		});

		helpMenu.getItems().add(aboutNotebookMenuItem);


		
		
		menuBar = new MenuBar();
//		menuBar.getMenus().setAll(fileMenu, editMenu, viewMenu, insertMenu, formatMenu, shareMenu, toolsMenu, tableMenu, windowMenu, helpMenu);
		menuBar.useSystemMenuBarProperty().set(true);
		menuBar.getMenus().setAll(fileMenu, toolsMenu, helpMenu);
	}
}
