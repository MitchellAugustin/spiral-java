package com.mitchellaugustin.notebook.utils;

public class Constants {
    public static final String VERSION = "Alpha_1.2";
    public static final int DEFAULT_TEXTBOX_WIDTH = 800;
    public static final int DEFAULT_TEXTBOX_HEIGHT = 600;

    public static final String EXPLAIN_CHOICES_URL = "https://mitchellaugustin.com/spiral/manual.html";
    public static final String HELP_URL = "https://mitchellaugustin.com/spiral/manual.html";
    public static final String KEYBOARD_SHORTCUTS_URL = "https://mitchellaugustin.com/spiral/manual.html";
    public static final String CONTRIBUTE_URL = "https://mitchellaugustin.com/spiral/contribute.html";
    public static final String UPDATE_CHECK_URL = "https://mitchellaugustin.com/spiral/downloads.html";

}
