package com.mitchellaugustin.notebook.utils;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.scene.web.HTMLEditor;

public class DragResizer {
	
    private static final int RESIZE_MARGIN = 30;

    private final Region region;

    private double y;
    private double x;
    
    private boolean initMinHeight;
    private boolean initMinWidth;
    
    private boolean dragging;
    
    private DragResizer(Region aRegion) {
        region = aRegion;
    }

    public static void makeResizable(Region region, HTMLEditor editor, Runnable onResizeCompleteRunnable) {
        final DragResizer resizer = new DragResizer(region);
        
        region.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                resizer.mousePressedY(event);
                resizer.mousePressedX(event);
            }});
        region.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                resizer.mouseDraggedY(event, editor);
                resizer.mouseDraggedX(event, editor);
            }});
        region.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                resizer.mouseOver(event);
            }});
        region.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                resizer.mouseReleased(event, editor);
                onResizeCompleteRunnable.run();
            }});
    }

    protected void mouseReleased(MouseEvent event, HTMLEditor htmlEditor) {
        dragging = false;
        region.setCursor(Cursor.DEFAULT);
        HTMLEditorUtils.resizeWebViewX(htmlEditor, true);
        HTMLEditorUtils.resizeWebViewY(htmlEditor, true);
    }

    protected void mouseOver(MouseEvent event) {
    	if((isInDraggableZoneY(event) && isInDraggableZoneX(event))) {
            region.setCursor(Cursor.SE_RESIZE);
    	}
    	else if(isInDraggableZoneY(event) || dragging) {
            region.setCursor(Cursor.S_RESIZE);
        }
        else if(isInDraggableZoneX(event) || dragging) {
            region.setCursor(Cursor.H_RESIZE);
        }
        else {
            region.setCursor(Cursor.DEFAULT);
        }
    }

    protected boolean isInDraggableZoneY(MouseEvent event) {
        return event.getY() > (region.getHeight() - RESIZE_MARGIN);
    }
    
    protected boolean isInDraggableZoneX(MouseEvent event) {
        return event.getX() > (region.getWidth() - RESIZE_MARGIN);
    }

    protected void mouseDraggedY(MouseEvent event, HTMLEditor editor) {
        if(!dragging) {
            return;
        }

        if(isInDraggableZoneY(event)) {
    		HTMLEditorUtils.resizeWebViewY(editor, false);
        }
		
        double mousey = event.getY();
        
        double newHeight = region.getMinHeight() + (mousey - y);

        region.setMinHeight(newHeight);
        
        y = mousey;
    }
    
    protected void mouseDraggedX(MouseEvent event, HTMLEditor editor) {
        if(!dragging) {
            return;
        }

        if(isInDraggableZoneX(event)) {
    		HTMLEditorUtils.resizeWebViewX(editor, false);
        }
		
        double mousex = event.getX();
        
        double newWidth = region.getMinWidth() + (mousex - x);

        region.setMinWidth(newWidth);
        
        x = mousex;
    }

    protected void mousePressedY(MouseEvent event) {
        
        if(!isInDraggableZoneY(event)) {
            return;
        }
        
        dragging = true;
        
        //Setting a min height that is smaller than the current height will have no effect
        if (!initMinHeight) {
            region.setMinHeight(region.getHeight());
            initMinHeight = true;
        }
        
        y = event.getY();
    }
    
    protected void mousePressedX(MouseEvent event) {
        
        if(!isInDraggableZoneX(event)) {
            return;
        }
        
        dragging = true;
        
        //Setting a min width that is smaller than the current height will have no effect
        if (!initMinWidth) {
            region.setMinWidth(region.getWidth());
            initMinWidth = true;
        }
        
        x = event.getX();
    }
}