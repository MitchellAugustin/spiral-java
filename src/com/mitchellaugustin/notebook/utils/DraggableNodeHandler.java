package com.mitchellaugustin.notebook.utils;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public class DraggableNodeHandler {
	public static final String CLASS_NAME = "DraggableNodeHandler";
	
	Node originalNode;
	Node node;
	boolean isDraggable = false;
	double nodeX;
	double nodeY;
	private Pane editorPane;
	private Runnable onDragCompleteRunnable;
	private Runnable onAnyMouseEventRunnable;
	private Runnable onMousePressedRunnable;
	
	public DraggableNodeHandler(final Node node, Pane editorPane) {
		originalNode = node;
		this.editorPane = editorPane;
	}
	
    public Node makeDraggable() {
    	node = originalNode;
    	isDraggable = true;
    	
        final DragContext dragContext = new DragContext();
        final Group wrapGroup = new Group(node);
        
        wrapGroup.addEventFilter(
                MouseEvent.ANY,
                new EventHandler<MouseEvent>() {
                    public void handle(final MouseEvent mouseEvent) {
                        //Disable mouse events for all children
                        mouseEvent.consume();
                        onAnyMouseEvent();
                    }
                });

        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_PRESSED,
                new EventHandler<MouseEvent>() {
                    public void handle(final MouseEvent mouseEvent) {
                        //Remember initial mouse cursor coordinates and node position
                        dragContext.mouseAnchorX = mouseEvent.getX();
                        dragContext.mouseAnchorY = mouseEvent.getY();
                        dragContext.initialTranslateX =
                                node.getTranslateX();
                        dragContext.initialTranslateY =
                                node.getTranslateY();
                        onMousePressed();
                    }
                });

        wrapGroup.addEventFilter(
                MouseEvent.MOUSE_DRAGGED,
                new EventHandler<MouseEvent>() {
                    public void handle(final MouseEvent mouseEvent) {
                        //Shift node from its initial position by delta calculated from mouse cursor movement
                        node.setTranslateX(
                                dragContext.initialTranslateX
                                    + mouseEvent.getX()
                                    - dragContext.mouseAnchorX);
                        node.setTranslateY(
                                dragContext.initialTranslateY
                                    + mouseEvent.getY()
                                    - dragContext.mouseAnchorY);
                        
                        nodeX = dragContext.initialTranslateX
                                + mouseEvent.getX()
                                - dragContext.mouseAnchorX;
                        nodeY = dragContext.initialTranslateY
                                + mouseEvent.getY()
                                - dragContext.mouseAnchorY;
                    }
                });
        
        wrapGroup.addEventFilter(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
			    double heightBeforeUndraggable = editorPane.getHeight();
			    double widthBeforeUndraggable = editorPane.getWidth();
				editorPane.getChildren().add(makeUndraggable());
				editorPane.setPrefHeight(heightBeforeUndraggable);
				editorPane.setPrefWidth(widthBeforeUndraggable);
				onDragComplete();
			}
        	
        });
                
        return wrapGroup;
    }
    
    public Node makeUndraggable() {
    	isDraggable = false;
    	double calculatedHeight = getNodeY() + 600;
    	double calculatedWidth = getNodeX() + 800;
    	editorPane.setPrefHeight(calculatedHeight > editorPane.getHeight() ? calculatedHeight : editorPane.getHeight());
    	editorPane.setPrefWidth(calculatedWidth > editorPane.getWidth() ? calculatedWidth : editorPane.getWidth());
    	return originalNode;
    }
    
    public boolean isDraggable() {
    	return isDraggable;
    }
    
    public double getNodeX() {
    	return nodeX;
    }
    
    public double getNodeY() {
    	return nodeY;
    }

    public void setLocation(double x, double y) {
    	nodeX = x;
    	nodeY = y;
    	
    	node.setTranslateX(x);
    	node.setTranslateY(y);
    }
    
    public void setOnDragComplete(Runnable r) {
    	onDragCompleteRunnable = r;
    }
    
    private void onDragComplete() {
    	if(onDragCompleteRunnable != null) {
        	onDragCompleteRunnable.run();
    	}
    }
    
    public void setOnAnyMouseEvent(Runnable r) {
    	onAnyMouseEventRunnable = r;
    }
    
    private void onAnyMouseEvent() {
    	if(onAnyMouseEventRunnable != null) {
        	onAnyMouseEventRunnable.run();
    	}
    }
    
    public void setOnMousePressed(Runnable r) {
    	onMousePressedRunnable = r;
    }
    
    private void onMousePressed() {
    	if(onMousePressedRunnable != null) {
        	onMousePressedRunnable.run();
    	}
    }
    
    private static final class DragContext {
        public double mouseAnchorX;
        public double mouseAnchorY;
        public double initialTranslateX;
        public double initialTranslateY;
    }
}
