package com.mitchellaugustin.notebook.utils;

import java.io.File;

public class FileManager {
	public static String CLASS_NAME = "FileManager";

	private static String appDirectory = "";
	private static String sessionFileLocation = "";
	
	public FileManager() {
		appDirectory = System.getProperty("user.dir");
		sessionFileLocation = appDirectory + File.separator + "session.json";
		Log.info("App directory initialized to: " + appDirectory, CLASS_NAME);
	}

	
	public String getAppDirectory() {
		return appDirectory;
	}

	public String getSessionFileLocation(){
		return sessionFileLocation;
	}
}
