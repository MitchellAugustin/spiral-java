package com.mitchellaugustin.notebook.utils;

import javafx.scene.Node;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.util.regex.Pattern;

public class HTMLEditorUtils {
	public static final String CLASS_NAME = "HTMLEditorUtils";
	
	public static void setToolbarVisibility(HTMLEditor htmlEditor, boolean visible) {
		Node topToolbar = htmlEditor.lookup(".top-toolbar");
		topToolbar.setManaged(visible);
		topToolbar.setVisible(visible);

		Node bottomToolbar = htmlEditor.lookup(".bottom-toolbar");
		bottomToolbar.setManaged(visible);
		bottomToolbar.setVisible(visible);
	}
	
	public static void resizeWebViewY(HTMLEditor htmlEditor, boolean isFinal) {
		int buffer = (isFinal ? 0 : 30);
		
		WebView webView = (WebView) htmlEditor.lookup(".web-view");
		webView.setPrefHeight(htmlEditor.getHeight() - buffer);
		webView.setMaxHeight(htmlEditor.getHeight() - buffer);
	}
	
	public static void resizeWebViewX(HTMLEditor htmlEditor, boolean isFinal) {
		int buffer = (isFinal ? 0 : 30);
		
		WebView webView = (WebView) htmlEditor.lookup(".web-view");
		webView.setPrefWidth(htmlEditor.getWidth() - buffer);
		webView.setMaxWidth(htmlEditor.getWidth() - buffer);
	}

	public static void wrapInDiv(HTMLEditor htmlEditor){
		Document doc = Jsoup.parse(htmlEditor.getHtmlText());
		if (doc.select("div").first() == null || doc.select("div").first().html().isEmpty()) {
			Element originalBody = doc.select("body").first();
			htmlEditor.setHtmlText("<html><div id=\"measured\"><body>" + originalBody.html() + "</body></div></html>");
		}
		else{
			Element originalDiv = doc.select("div").first();
			htmlEditor.setHtmlText("<html><div id=\"measured\"><body>" + originalDiv.html() + "</body></div></html>");
		}
	}

	//To anyone reading this, I just want to let you know that this method is not representative of my ability as a programmer.
	public static void addImage(HTMLEditor htmlEditor, String imgSrc){
		imgSrc = "file:///" + imgSrc.replaceAll(Pattern.quote(File.separator), "/");
		WebView webView = (WebView) htmlEditor.lookup(".web-view");
		WebEngine engine = webView.getEngine();
		String script = "function insertHtmlAtCursor(html) {\n" +
				"    var range, node;\n" +
				"    if (window.getSelection && window.getSelection().getRangeAt) {\n" +
				"        range = window.getSelection().getRangeAt(0);\n" +
				"        node = range.createContextualFragment(html);\n" +
				"        range.insertNode(node);\n" +
				"    } else if (document.selection && document.selection.createRange) {\n" +
				"        document.selection.createRange().pasteHTML(html);\n" +
				"    }\n" +
				"}" +
				"insertHtmlAtCursor('<img src=\"" + imgSrc + "\"/>');";
		engine.executeScript(script);
	}

	public static boolean isEmpty(HTMLEditor htmlEditor){
		Document doc = Jsoup.parse(htmlEditor.getHtmlText());
		boolean isEmpty = doc.select("div").first() == null || doc.select("div").first().html().isEmpty();
		return isEmpty;
	}
}
