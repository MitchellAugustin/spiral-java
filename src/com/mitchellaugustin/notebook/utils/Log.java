package com.mitchellaugustin.notebook.utils;

/**
 * Logger class
 * @author Mitchell Augustin
 *
 * TODO Add output to log file
 */
public class Log {
	public static void info(String message, String className) {
		System.out.println("[INFO/" + className + "]: " + message);
	}
	
	public static void info(String message) {
		System.out.println("[INFO/" + "UNKNOWN" + "]: " + message);
	}
	
	public static void warn(String message, String className) {
		System.out.println("[WARN/" + className + "]: " + message);
	}
	
	public static void error(String message, String className) {
		System.out.println("[ERROR/" + className + "]: " + message);
	}
}
