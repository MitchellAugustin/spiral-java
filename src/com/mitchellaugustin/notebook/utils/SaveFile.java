package com.mitchellaugustin.notebook.utils;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;

public class SaveFile {
    public static final String CLASS_NAME = "SaveFile";
    /**
     * Saves the specified text to the specified file
     * @param filename - The file to save the text to
     * @param filetext - The text to write
     */
    public static void saveFile(String filename, String filetext){
        PrintWriter writer;
        try {
            if(new File(filename).exists()){
                Log.info("Saving " + filename, CLASS_NAME);
                writer = new PrintWriter(filename, "UTF-8");
                writer.println(filetext);
                writer.close();
            }
            else{
                try {
                    new File(filename).createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.info("Saving " + filename, CLASS_NAME);
                writer = new PrintWriter(filename.trim(), "UTF-8");
                writer.println(filetext);
                writer.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void saveImageToFile(String path, Image image) {
        File outputFile = new File(path);
        BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
        try {
            ImageIO.write(bImage, path.split("\\.")[path.split("\\.").length - 1], outputFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String readFile(String filename){
        String file = "Unavailible";
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(filename));
            file = new String(encoded, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static File[] listFiles(){
        File folder = new File(System.getProperty("user.dir"));
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println("File " + listOfFiles[i].getName());
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
            }
        }
        return listOfFiles;
    }
}
