package com.mitchellaugustin.notebook.utils;

import javafx.application.Application;
import javafx.application.HostServices;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

public class URLHandler extends URLStreamHandler {
    private Application application;

    public URLHandler(Application application) {
        this.application = application;
    }

    @Override
    protected URLConnection openConnection(URL u) throws IOException {
        return new HostServicesUrlConnection(u, application.getHostServices());
    }
}

class HostServicesUrlConnection extends HttpURLConnection {

    private URL urlToOpen;
    private HostServices hostServices;

    protected HostServicesUrlConnection(URL u, HostServices hostServices) {
        super(u);
        this.urlToOpen= u;
        this.hostServices = hostServices;
    }

    @Override
    public void disconnect() {
        // do nothing
    }

    @Override
    public boolean usingProxy() {
        return false;
    }

    @Override
    public void connect() throws IOException {
        hostServices.showDocument(urlToOpen.toExternalForm());
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new InputStream() {

            @Override
            public int read() throws IOException {
                return 0;
            }
        };
    }

}